package mermaid.design.pattern.practice.state;

import java.util.Collections;
import java.util.List;

public class HasFuel implements State{
    public SteamworkFacility steamworkFacility;
    
    public HasFuel(SteamworkFacility steamworkFacility) {
        this.steamworkFacility = steamworkFacility; 
    }
    
    @Override
    public void addFuel(int units) {
        this.steamworkFacility.setUnitsOfFuel(this.steamworkFacility.getUnitsOfFuel() + units);
        System.out.printf(
            "Add %d units of fuels into the machine, %d units of fuels are in the machine now!%n", 
            units, 
            this.steamworkFacility.getUnitsOfFuel()
        );
    }

    @Override
    public void turnCrank() {
        List<SteamworkFacility.Button> buttons = this.steamworkFacility.getButtonsWaitForInput();
        Collections.shuffle(buttons);
        System.out.printf(
            "Follow the order and Input %s(%s), %s(%s), %s(%s) so as to get more bouns %n", 
            buttons.get(0), buttons.get(0).toString().charAt(0), 
            buttons.get(1), buttons.get(1).toString().charAt(0), 
            buttons.get(2), buttons.get(2).toString().charAt(0) 
        );
        this.steamworkFacility.setUnitsOfFuel(this.steamworkFacility.getUnitsOfFuel() - 10);
        this.steamworkFacility.setState(this.steamworkFacility.getWaitForInputState());
    }
    

    @Override
    public void enterKey() {
        System.out.println("Please turn the crank first!");
    }

    @Override
    public void offerReward() {
        System.out.println("Please turn the crank first!");
    }


}
