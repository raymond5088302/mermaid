package mermaid.design.pattern.practice.state;

/* These states are used by Context that has more handle methods so as to represent it's internal state and behavior. 
 * The "State objects" don't keep their own states, all changes will save in Context. They may be an instance or a primitive type
 * */
public interface State {
    
    public void addFuel(int units);
    public void turnCrank();
    public void enterKey();
    public void offerReward();

}
