package mermaid.design.pattern.practice.state;


/*
 * Idea from https://www.youtube.com/watch?v=V5d7kM6UVV4
 * */
public class Main {

    public static void main(String[] args) {
        SteamworkFacility steamworkFacility = new SteamworkFacility(100);
        steamworkFacility.addFuel(50);
//        steamworkFacility.enterKey();
//        steamworkFacility.offerReward();

        for (int i = 0; i < 10; i++) {
            steamworkFacility.turnCrank();
            steamworkFacility.enterKey();
            steamworkFacility.offerReward();
        }
    }
}
