package mermaid.design.pattern.practice.state;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/* This class is the Context */
public class SteamworkFacility {
    public enum Button {
        TRIANGLE, CROSS, SQUARE
    }
    public enum Reward {
        MAXPOTION, ARMORSPHERE, TRANQBOMB
    }
    private int unitsOfFuel;
    private int pointsOfBonus;
    private List<Button> buttonsWaitForInput;
    private List<Reward> allKindsOfRewards;
    private List<Reward> receivedRewards;
    private State state;
    private State hasNoFuelState;
    private State hasFuelState;
    private State offerRewardState;
    private State waitForInputState;
    private Scanner scanner;

    public SteamworkFacility(int unitsOfFuel) {
        this.buttonsWaitForInput = Arrays.asList(new Button[] {Button.TRIANGLE, Button.CROSS, Button.SQUARE});
        this.allKindsOfRewards = Arrays.asList(new Reward[] {Reward.MAXPOTION, Reward.ARMORSPHERE, Reward.TRANQBOMB});
        this.receivedRewards = new ArrayList<Reward>();
        this.hasNoFuelState = new HasNoFuel(this);
        this.hasFuelState = new HasFuel(this);
        this.waitForInputState = new WaitForInput(this);
        this.offerRewardState = new OfferReward(this); 
        if (unitsOfFuel > 0) {
            this.unitsOfFuel = unitsOfFuel;
            this.state = this.hasFuelState;
        } else {
            this.state = this.hasNoFuelState;
        }
        this.scanner = new Scanner(System.in);
    }
    
    public void addFuel(int units) {
        this.state.addFuel(units);
    }
    public void turnCrank() {
        this.state.turnCrank();
    }
    public void enterKey() { 
        this.state.enterKey();
    }
    public void offerReward() {
        this.state.offerReward();
    }
    public int getUnitsOfFuel() {
        return unitsOfFuel;
    }
    public void setUnitsOfFuel(int unitsOfFuel) {
        this.unitsOfFuel = unitsOfFuel;
    }
    public int getPointsOfBonus() {
        return pointsOfBonus;
    }
    public void setPointsOfBonus(int pointsOfBonus) {
        this.pointsOfBonus = pointsOfBonus;
    }
    public List<Button> getButtonsWaitForInput() {
        return buttonsWaitForInput;
    }
    public void setButtonsWaitForInput(List<Button> buttonsWaitForInput) {
        this.buttonsWaitForInput = buttonsWaitForInput;
    }
    public List<Reward> getAllKindsOfRewards() {
        return allKindsOfRewards;
    }
    public void setAllKindsOfRewards(List<Reward> allKindsOfRewards) {
        this.allKindsOfRewards = allKindsOfRewards;
    }
    public State getState() {
        return state;
    }
    public void setState(State state) {
        this.state = state;
    }
    public State getHasNoFuelState() {
        return hasNoFuelState;
    }
    public void setHasNoFuelState(State hasNoFuelState) {
        this.hasNoFuelState = hasNoFuelState;
    }
    public State getHasFuelState() {
        return hasFuelState;
    }
    public void setHasFuelState(State hasFuelState) {
        this.hasFuelState = hasFuelState;
    }
    public State getOfferRewardState() {
        return offerRewardState;
    }
    public void setOfferRewardState(State offerRewardState) {
        this.offerRewardState = offerRewardState;
    }
    public State getWaitForInputState() {
        return waitForInputState;
    }
    public void setWaitForInputState(State waitForInputState) {
        this.waitForInputState = waitForInputState;
    }
    public List<Reward> getReceivedRewards() {
        return receivedRewards;
    }
    public void setReceivedRewards(List<Reward> receivedRewards) {
        this.receivedRewards = receivedRewards;
    }
    public Scanner getScanner() {
        return scanner;
    }
    public void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }

}
