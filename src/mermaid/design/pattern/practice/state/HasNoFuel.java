package mermaid.design.pattern.practice.state;

public class HasNoFuel implements State{
    public SteamworkFacility steamworkFacility;
    
    public HasNoFuel(SteamworkFacility steamworkFacility) {
        this.steamworkFacility = steamworkFacility; 
    }
    
    @Override
    public void addFuel(int units) {
        this.steamworkFacility.setUnitsOfFuel(units);
        this.steamworkFacility.setState(steamworkFacility.getHasFuelState());
        System.out.printf(
            "Add %d units of fuels into machine, %d units of fuels are in the machine now!", 
            units, 
            this.steamworkFacility.getUnitsOfFuel()
        );
    }

    @Override
    public void turnCrank() {
        System.out.println("The machine ran out of fuels, add some fuels please!");
    }
    

    @Override
    public void enterKey() {
        System.out.println("The machine ran out of fuels, add some fuels please!");
    }

    @Override
    public void offerReward() {
        System.out.println("The machine ran out of fuels, add some fuels please!");
    }

}
