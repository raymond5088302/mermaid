package mermaid.design.pattern.practice.state;

import java.util.Collections;

public class OfferReward implements State{
    public SteamworkFacility steamworkFacility;
    
    public OfferReward(SteamworkFacility steamworkFacility) {
        this.steamworkFacility = steamworkFacility; 
    }
    
    @Override
    public void addFuel(int units) {
        System.out.println("Please receive your rewards first!");
    }

    @Override
    public void turnCrank() {
        System.out.println("Please receive your rewards first!");
    }
    

    @Override
    public void enterKey() {
        System.out.println("Please receive your rewards first!");
    }

    @Override
    public void offerReward() {
        if (this.steamworkFacility.getPointsOfBonus() >= 100) {
            this.steamworkFacility.getReceivedRewards().addAll(this.steamworkFacility.getAllKindsOfRewards());
            this.steamworkFacility.setPointsOfBonus(0);
            System.out.printf("The machine over heat, so it start giving your more rewards. All reward you own: %s%n", this.steamworkFacility.getReceivedRewards());
        } else {
            Collections.shuffle(this.steamworkFacility.getAllKindsOfRewards());
            this.steamworkFacility.getReceivedRewards().add(this.steamworkFacility.getAllKindsOfRewards().get(0));
            System.out.printf("You get one reward. All reward you own: %s%n", this.steamworkFacility.getReceivedRewards());
        }
        
        if (this.steamworkFacility.getUnitsOfFuel() > 0) {
            this.steamworkFacility.setState(this.steamworkFacility.getHasFuelState());
        } else {
            this.steamworkFacility.setState(this.steamworkFacility.getHasNoFuelState());
        }
    }

}
