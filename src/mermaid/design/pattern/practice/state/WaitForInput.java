package mermaid.design.pattern.practice.state;

import java.util.ArrayList;
import java.util.List;

import mermaid.design.pattern.practice.state.SteamworkFacility.Button;

public class WaitForInput implements State{
    public SteamworkFacility steamworkFacility;
    
    public WaitForInput(SteamworkFacility steamworkFacility) {
        this.steamworkFacility = steamworkFacility; 
    }
    
    @Override
    public void addFuel(int units) {
        System.out.println("Please press buttons first!");
    }

    @Override
    public void turnCrank() {
        System.out.println("Please press buttons first!"); 
    }

    @Override
    public void enterKey() {
        List<Button> inputs = new ArrayList<Button>();
        for (int i = 0; i < 3; i++) {
            switch(this.steamworkFacility.getScanner().next().toUpperCase().charAt(0)) {
                default:
                case 'T':
                    inputs.add(Button.TRIANGLE);
                    break;
                case 'S':
                    inputs.add(Button.SQUARE);
                    break;
                case 'C':
                    inputs.add(Button.CROSS);
                    break; 
            }
        }
        if (this.steamworkFacility.getButtonsWaitForInput().equals(inputs)) {
            this.steamworkFacility.setPointsOfBonus(this.steamworkFacility.getPointsOfBonus() + 20);
            System.out.printf(
                "Your inputs are currect, so you get more points for bouns. The machine has %d points now!%n", 
                this.steamworkFacility.getPointsOfBonus());
        } else {
            this.steamworkFacility.setPointsOfBonus(this.steamworkFacility.getPointsOfBonus() + 10);
            System.out.printf(
                "Your inputs are not currect, so you get less points for bouns. The machine has %d points now!%n", 
                this.steamworkFacility.getPointsOfBonus());
        }
        this.steamworkFacility.setState(this.steamworkFacility.getOfferRewardState());
    }

    @Override
    public void offerReward() {
        System.out.println("Please press buttons first!"); 
    }

}
