package mermaid.design.pattern.practice.composite;

public class WeaponItem extends WeaponComponent{
    public String name;
    public int rare;
    public int damage;
    
    public WeaponItem(String name, int rare, int damage) {
        super();
        this.name = name;
        this.rare = rare;
        this.damage = damage;
    }

    public String getName() {
        return this.name;
    }
    
    public int getDamage() {
        return this.damage;
    }
    
    public int getRare() {
        return this.rare;
    }
    
    public void print() {
        for (int i = 1; i <= rare; i++) {
            System.out.print("----");
        }
        System.out.printf("%s (Rare: %d, Damage: %d)%n", this.name, this.rare, this.damage);
    }
}
