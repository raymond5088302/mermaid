package mermaid.design.pattern.practice.composite;

/**
 * idea from https://monsterhunter.fandom.com/wiki/MHW:_Charge_Blade_Weapon_Tree
 * idea from https://youtu.be/KlFH6onxTdU?t=887
 */
public class Main {

    public static void main(String[] args) {
        WeaponComponent protoCommissionAxeLv1Tree = new WeaponTree("Proto Commission Axe I Tree", 1);
        WeaponComponent protoCommissionAxeLv1Weapon = new WeaponItem("Proto Commission Axe I", 1, 288);
        WeaponComponent MudslideBladeLv1Weapon = new WeaponItem("Mudslide Blade I", 1, 396);
        protoCommissionAxeLv1Tree.add(protoCommissionAxeLv1Weapon);
        protoCommissionAxeLv1Tree.add(MudslideBladeLv1Weapon);
        WeaponComponent protoCommissionAxe2Tree = new WeaponTree("Proto Commission Axe II Tree", 2);
        WeaponComponent protoCommissionAxe2Weapon = new WeaponItem("Proto Commission Axe II", 2, 324);
        protoCommissionAxeLv1Tree.add(protoCommissionAxe2Tree);
        protoCommissionAxe2Tree.add(protoCommissionAxe2Weapon);
        WeaponComponent protoCommissionAxe3Tree = new WeaponTree("Proto Commission Axe III Tree", 3);   
        WeaponComponent protoCommissionAxe3Weapon = new WeaponItem("Proto Commission Axe III", 3, 360);
        protoCommissionAxe2Tree.add(protoCommissionAxe3Tree);
        protoCommissionAxe3Tree.add(protoCommissionAxe3Weapon);
        WeaponComponent boneStrongarmLv1Tree = new WeaponTree("Bone Strongarm I Tree", 1);
        WeaponComponent boneStrongarmLv1Weapon = new WeaponItem("Bone Strongarm I", 1, 324);
        boneStrongarmLv1Tree.add(boneStrongarmLv1Weapon);
        WeaponComponent boneStrongarmLv2Tree = new WeaponTree("Bone Strongarm II Tree", 2);
        boneStrongarmLv1Tree.add(boneStrongarmLv2Tree);
        WeaponComponent boneStrongarmLv2Weapon = new WeaponItem("Bone Strongarm II", 2, 360);
        boneStrongarmLv2Tree.add(boneStrongarmLv2Weapon);
        WeaponComponent jpagrasStrongarmLv1Weapon = new WeaponItem("Jagras Strongarm I", 2, 394);
        boneStrongarmLv2Tree.add(jpagrasStrongarmLv1Weapon);
        WeaponComponent pulsarStrongarmLv1Weapon = new WeaponItem("Pulsar Strongarm I", 2, 396);
        boneStrongarmLv2Tree.add(pulsarStrongarmLv1Weapon);
        
        WeaponComponent root = new WeaponTree("Root", 0);
        root.add(protoCommissionAxeLv1Tree);
        root.add(boneStrongarmLv1Tree);
        WeaponRoot weaponRoot = new WeaponRoot(root);
        
        weaponRoot.printAllWeaponFromRoot();
    }

}
