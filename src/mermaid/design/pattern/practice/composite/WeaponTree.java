package mermaid.design.pattern.practice.composite;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class WeaponTree extends WeaponComponent{
    List<WeaponComponent> weapons;
    public String name;
    public int rare;
    
    public WeaponTree(String name, int rare) {
        super();
        this.weapons = new ArrayList<WeaponComponent>();
        this.name = name;
        this.rare = rare;
    }
    
    public void add(WeaponComponent weaponComponent) {
        this.weapons.add(weaponComponent);
    }
    
    public void remove(WeaponComponent weaponComponent) {
        this.weapons.remove(weaponComponent);
    }
    
    public void get(int index) {
        this.weapons.get(index);
    }

    public String getName() {
        return this.name;
    }
    
    public int getRare() {
        return this.rare;
    }
    
    public void print() {
//        for (int i = 0; i < rare; i++) {
//            System.out.print("----");
//        }
//        System.out.println(this.name);
        Iterator<WeaponComponent> iterator = weapons.iterator();
        while(iterator.hasNext()) {
            iterator.next().print();
        }
    }
}
