package mermaid.design.pattern.practice.composite;

public abstract class WeaponComponent {
    public void add(WeaponComponent weaponComponent) {
        throw new UnsupportedOperationException();
    }
    
    public void remove(WeaponComponent weaponComponent) {
        throw new UnsupportedOperationException();
    }
    
    public WeaponComponent getChild(int i) {
        throw new UnsupportedOperationException();
    }
    
    public String getName() {
        throw new UnsupportedOperationException();
    }
    
    public int getDamage() {
        throw new UnsupportedOperationException();
    }
    
    public int getRare() {
        throw new UnsupportedOperationException();
    }
    
    public void print() {
        throw new UnsupportedOperationException();
    }
}
