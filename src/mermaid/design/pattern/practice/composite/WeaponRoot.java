package mermaid.design.pattern.practice.composite;

public class WeaponRoot {
    WeaponComponent root;
    
    public WeaponRoot(WeaponComponent root) {
        this.root = root;
    }
    
    public void printAllWeaponFromRoot() {
        System.out.println("Root");
        this.root.print();
    }
}
