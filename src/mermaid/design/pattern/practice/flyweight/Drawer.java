package mermaid.design.pattern.practice.flyweight;

import java.awt.BorderLayout;
import java.awt.Graphics;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class Drawer extends JFrame {
    public JPanel canvasPanel;
    
    public Drawer() {

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        this.canvasPanel = new JPanel();
        
        JPanel bottomPanel = new JPanel();
        bottomPanel.setLayout(new BorderLayout());
        
        JButton drawBtn = new JButton("Start Drawing");
        bottomPanel.add(drawBtn, BorderLayout.EAST);

        JPanel optionPanel = new JPanel();
        optionPanel.setLayout(new BorderLayout());
        JCheckBox flyweightBox = new JCheckBox("use flyweight");
        SpinnerNumberModel numberModel = new SpinnerNumberModel(5, 1, Integer.MAX_VALUE, 10000);
        JSpinner quantitySpinner = new JSpinner(numberModel);
        optionPanel.add(flyweightBox, BorderLayout.EAST);
        optionPanel.add(quantitySpinner, BorderLayout.CENTER);
        bottomPanel.add(optionPanel, BorderLayout.WEST);
   
        JPanel infoPanel = new JPanel();
        infoPanel.setLayout(new BorderLayout());
        JLabel calculatedTime = new JLabel("  Spending Time: 0ms");
        infoPanel.add(calculatedTime, BorderLayout.CENTER);
        bottomPanel.add(infoPanel, BorderLayout.CENTER);
        
        mainPanel.add(canvasPanel, BorderLayout.CENTER);
        mainPanel.add(bottomPanel, BorderLayout.SOUTH);
        this.setSize(1024, 768);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Swords");
        this.add(mainPanel);
        this.setVisible(true);
        
        drawBtn.addActionListener(
            event -> {
                int quantity = (int)quantitySpinner.getValue();
                Graphics g = this.canvasPanel.getGraphics();
                g.clearRect(0, 0, this.canvasPanel.getWidth(), this.canvasPanel.getHeight());
                Forge forge = new Forge();
                
                long startTime = System.currentTimeMillis();
                if (flyweightBox.isSelected()) {
                    for (int i = 0; i < quantity; i++) {
                        
                        /* Reduce the number of instances at run time. */
                        forge.designBluePrint(Forge.Category.LONGSWORD).paint(g);
                        forge.designBluePrint(Forge.Category.SHORTSWORD).paint(g);
                    }
                } else {
                    for (int i = 0; i < quantity; i++) {
                        
                        /* Create instances every time when draw a new sword at run time. */
                        new Sword(Forge.Category.LONGSWORD).paint(g);
                        new Sword(Forge.Category.SHORTSWORD).paint(g);
                    }
                }
                long endTime = System.currentTimeMillis();
                calculatedTime.setText("  Spending Time: " + (endTime - startTime) + "ms");
            }
        );
    }
}
