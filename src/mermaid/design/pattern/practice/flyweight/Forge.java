package mermaid.design.pattern.practice.flyweight;

import java.util.HashMap;
import java.util.Map;

public class Forge {
    public enum Category {
        LONGSWORD, SHORTSWORD;   
    }
    
    /* Use the feature of Map to control the number of instances and collect virtual objects into one place. */
    private Map<Category, Sword> blueprints;
    
    public Forge() {
        this.blueprints = new HashMap<Category,Sword>();
    }
    
    /* Centralize state for virtual objects into single location. */
    public Sword designBluePrint(Category category) {
        Sword sword = this.blueprints.get(category);
        if (sword == null) {
            sword = new Sword(category);
            this.blueprints.put(category, sword);
        }
        
        return sword;
    }
    
}
