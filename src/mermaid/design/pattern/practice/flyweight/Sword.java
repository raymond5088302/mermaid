package mermaid.design.pattern.practice.flyweight;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Sword {
    private int[] xPoints;
    private int[] yPoints;
    private double rate;
    public String generatedDateTime;
    
    /* Some variables, operation and behavior that don't need to be changed independently. Sometime they may waste memory. */
    public Sword(Forge.Category category) {
        switch(category) {
            default:
            case LONGSWORD:
                this.xPoints = new int[] {19, 30, 441, 460, 450, 458, 475, 569, 600, 605, 600, 570, 475, 450, 459, 437, 31}; 
                this.yPoints = new int[] {80, 72, 65, 61, 19, 18, 70, 74, 67, 80, 93, 87, 90, 142, 100, 95, 87};
            break;
            case SHORTSWORD:
                this.xPoints = new int[] {10, 412, 421, 436, 544, 565, 577, 563, 541, 548, 465, 471, 465, 459, 461, 458, 453, 434, 411, 397, 366, 317, 302, 289, 231, 185, 149, 128, 70, 30, 23}; 
                this.yPoints = new int[] {341, 98, 82, 86, 21, 25, 37, 36, 58, 110, 118, 134, 174, 173, 156, 144, 133, 132, 136, 146, 166, 196, 207, 218, 261, 293, 313, 323, 341, 346, 349};
            break;
        }
        String pattern = "EEEEE MMMMM yyyy-MM-dd HH:mm:ss.SSSZ";
        SimpleDateFormat simpleDateFormat =new SimpleDateFormat(pattern, new Locale("tw"));
        String date = simpleDateFormat.format(new Date());
        this.generatedDateTime = date; 
    }

    
    public static Color getRandomColor() {
        switch((int)(Math.random() * 5)) {
            default:
            case 0:
                return Color.BLACK;
            case 1:
                return Color.GREEN;
            case 2:
                return Color.BLUE;
            case 3:
                return Color.YELLOW;
            case 4:
                return Color.MAGENTA;
            case 5:
                return Color.RED;
        }
    }
    
    /* Only maintain and change the necessary state here, so we don't have to spend hung cost for creating new object every time. */
    public void paint(Graphics g) {
        this.rate = Math.random();
        int xOffset =  (int)(Math.random() * 500);
        int yOffset =  (int)(Math.random() * 500);
        Polygon p = new Polygon();
        g.setColor(getRandomColor());
        for (int i = 0; i < this.xPoints.length; i++) {
            p.addPoint(
                (int)((xPoints[i] + xOffset) * rate), 
                (int)((yPoints[i] + yOffset) * rate)
            );
        }
        g.fillPolygon(p);
    }

}
