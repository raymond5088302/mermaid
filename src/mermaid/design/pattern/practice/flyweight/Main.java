package mermaid.design.pattern.practice.flyweight;

public class Main {
    
    /*
     * https://variety.com/2019/tv/reviews/witcher-netflix-1203434525/
     * https://en.wikipedia.org/wiki/The_Witcher_3:_Wild_Hunt
     * https://www.youtube.com/watch?v=ndl1W4ltcmg
     * */
    public static void main(String[] args) {
        new Drawer();  
    }

}
