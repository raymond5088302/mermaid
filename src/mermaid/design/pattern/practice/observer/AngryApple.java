package mermaid.design.pattern.practice.observer;

public class AngryApple extends Fruit {

    public AngryApple(String name, ISubject place) {
        super(name, "What is it!!", false, true, place);
    }
}
