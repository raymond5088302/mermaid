package mermaid.design.pattern.practice.observer;

public interface ISubject {
    public abstract void register(IObserver observer);
    public abstract void unregister(IObserver observer);
    public abstract void add(Fruit fruit);
    public abstract void remove(Fruit fruit);
    public abstract void notifyFruits();
}
