package mermaid.design.pattern.practice.observer;

import static java.lang.System.out;
import java.util.ArrayList;

public class KitchenTable implements ISubject{
    private ArrayList<Fruit> fruits;
    private ArrayList<IObserver> observers;
    
    public KitchenTable() {
        this.fruits = new ArrayList<Fruit>();
        this.observers = new ArrayList<IObserver>();
    }

    @Override
    public void register(IObserver observer) {
        this.observers.add(observer);
    }

    @Override
    public void unregister(IObserver observer) {
        int index = this.observers.indexOf(observer);
        out.printf("Observer %d deleted%n", (index + 1));
        this.observers.remove(index);
    }

    @Override
    public void add(Fruit fruit) {
        out.printf("%s is put on kitchen table!!%n", fruit.getName());
        this.fruits.add(fruit);
        notifyFruits();
    }

    @Override
    public void remove(Fruit fruit) {
        int index = this.fruits.indexOf(fruit);
        out.printf("%s removed from kitchen table", fruit.getName());
        this.fruits.remove(index);
    }

    @Override
    public void notifyFruits() {
        for (Fruit fruit : this.fruits) {
            for(IObserver observer : this.observers) {
                if (observer instanceof IAnnoyingObserver) {
                    ((IAnnoyingObserver) observer).update(fruit, fruits);
                } else if (observer instanceof IAngryObserver) {
                    ((IAngryObserver) observer).update(fruit, fruits);
                }
            }
        }
    }
}
