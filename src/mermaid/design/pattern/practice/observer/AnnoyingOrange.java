package mermaid.design.pattern.practice.observer;

public class AnnoyingOrange extends Fruit {

    public AnnoyingOrange(String name, ISubject place) {
        super(name, "Hey!!", true, false, place);
    }
}
