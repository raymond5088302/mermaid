package mermaid.design.pattern.practice.observer;

public class BoredPear extends Fruit {

    public BoredPear(String name, ISubject place) {
        super(name, "Oh! God!", false, true, place);
    }
}
