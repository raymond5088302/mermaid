package mermaid.design.pattern.practice.observer;

import static java.lang.System.out;
import java.util.ArrayList;

public class AngryObserver implements IAngryObserver {
    
    public AngryObserver() {
        
    }

    @Override
    public void update(Fruit myself, ArrayList<Fruit> members) {
        StringBuilder sb = new StringBuilder();
        if (myself.isAngry) {
            sb.append(myself.getName()).append(" is very angry......\n");
            for(Fruit member : members) {
                if (!member.equals(myself)) {
                    if (member.isAnnoying) {
                        sb.append(myself.getName()).append(" is angry at ").append(member.getName()).append("\n");                                            
                    }
                }
            }
            out.println(sb.toString());
        }
    }
}
