package mermaid.design.pattern.practice.observer;

public class Fruit {
    protected String name;
    protected String petPhrase;
    protected boolean isAnnoying;
    protected boolean isAngry;
    protected ISubject place;
    
    public Fruit(String name, String petPhrase, boolean isAnnoying, boolean isAngry, ISubject place) {
        this.name = name;
        this.petPhrase = petPhrase;
        this.isAnnoying = isAnnoying;
        this.isAngry = isAngry;
        this.place = place;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPetPhrase() {
        return petPhrase;
    }

    public void setPetPhrase(String petPhrase) {
        this.petPhrase = petPhrase;
    }

    public boolean isAnnoying() {
        return isAnnoying;
    }

    public void setAnnoying(boolean isAnnoying) {
        if (!this.isAnnoying) {
            if (isAnnoying) {
                this.isAnnoying = isAnnoying;
                System.out.printf("%s becomes annoying%n", this.getName());
                place.notifyFruits();
            }
        } else {
            this.isAnnoying = isAnnoying;
        }
        
    }

    public boolean isAngry() {
        return isAngry;
    }

    public void setAngry(boolean isAngry) {
        this.isAngry = isAngry;
    }  
}
