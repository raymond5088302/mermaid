package mermaid.design.pattern.practice.observer;

import static java.lang.System.out;

import java.util.ArrayList;

public class AnnoyingObserver implements IAnnoyingObserver {
    
    public AnnoyingObserver() {
        
    }

    @Override
    public void update(Fruit myself, ArrayList<Fruit> members) {
        StringBuilder sb = new StringBuilder();
        if (myself.isAnnoying()) {
            sb.append(myself.getName()).append(" start annoying other members......\n");
            for (Fruit member : members) {
                if (!myself.equals(member)) {
                    sb.append(myself.getName()).append(": ").append(myself.getPetPhrase()).append(" ")
                            .append(member.getName()).append("\n");
                }
            }
            out.println(sb.toString());
        }
    }
}
