package mermaid.design.pattern.practice.observer;

import java.util.ArrayList;

public interface IAngryObserver extends IObserver {
    public void update(Fruit myself, ArrayList<Fruit> members);
}
