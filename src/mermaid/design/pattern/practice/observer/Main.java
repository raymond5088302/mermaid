package mermaid.design.pattern.practice.observer;

/* Reference to https://www.youtube.com/watch?v=DD5UKQggXTc */
public class Main {

    public static void main(String[] args) {
        
        /* Register the observers (allow members to do something) */
        ISubject kitchenTable = new KitchenTable();
        IAngryObserver angryObserver = new AngryObserver();
        kitchenTable.register(angryObserver);
        IAnnoyingObserver annoyingOthers = new AnnoyingObserver();
        kitchenTable.register(annoyingOthers);
        
        /* Initial all members (fruits) */
        BoredPear boredPear = new BoredPear("Pear", kitchenTable);
        AngryApple angryApple = new AngryApple("Apple", kitchenTable);
        AnnoyingOrange annoyingOrange = new AnnoyingOrange("Orange1", kitchenTable);
        
        /* Register all memebers (Put all fruits on the kitchen table) */
        kitchenTable.add(annoyingOrange);
        System.out.println("\n");
        kitchenTable.add(angryApple);
        System.out.println("\n");
        kitchenTable.add(boredPear);
        System.out.println("\n");
        
        /* Trigger observers when the particular state is changed */
        angryApple.setAnnoying(true);
        System.out.println("\n");

        /* Trigger observers when putting new member on kitchen table */
        AnnoyingOrange annoyingOrange2 = new AnnoyingOrange("Orange2", kitchenTable);
        kitchenTable.add(annoyingOrange2);  
        
        /* Modify one of members state */
//        angryApple.setAngry(false);
        
        /* Observers can be replaced or removed any time. */
//        kitchenTable.unregister(beAngryAtOthers);
        
    }
}
