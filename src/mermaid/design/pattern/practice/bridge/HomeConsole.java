package mermaid.design.pattern.practice.bridge;

abstract class HomeConsole {
    public enum DeviceState {
        POWERUP, POWEROFF, SLEEP
    }
    
    protected DeviceState deviceState;
    protected int bright;
    
    public void turnScreenBrigter() {
        this.bright++;
    }
    
    public void turnScreenDarker() {
        this.bright--;
    }
    
    public void shutdown() {
        this.deviceState = DeviceState.POWEROFF;
    }
    
    public void start() {
        this.deviceState = DeviceState.POWERUP;
    }
    
    public void sleep() {
        this.deviceState = DeviceState.SLEEP;
    }
    
    public abstract void connectToStore();
}
