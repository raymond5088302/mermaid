package mermaid.design.pattern.practice.bridge;

abstract class Controller {
    protected HomeConsole homeConsole;
    
    public Controller(HomeConsole homeConsole) {
        this.homeConsole = homeConsole;
    }
    
    public void pressButtonStart() {
        this.homeConsole.start();
    }
    
    public void pressButtonShutdown() {
        this.homeConsole.shutdown();
    }
    
    public abstract void pressButtonStore();
}
