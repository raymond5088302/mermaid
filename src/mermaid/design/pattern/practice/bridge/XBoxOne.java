package mermaid.design.pattern.practice.bridge;

import java.util.Arrays;
import java.util.List;

public class XBoxOne extends HomeConsole {
    
    private List<String> listOfGame;
    
    public XBoxOne(int bright) {
        this.bright = bright;
        this.deviceState = DeviceState.POWEROFF;
        this.listOfGame = Arrays.asList(
            new String[] {"The Witcher 3", "Red Dead Redemption", "Assassin's Creed Odyssey", "Grand Theft Auto 5", "Resident Evil 7"}
        );
        System.out.println("Connect to XBoxOne now...");
    }

    @Override
    public void connectToStore() {
        System.out.println("Games in XBox Marketplace we have below:");
        this.listOfGame.stream().forEach(System.out::println);
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("XBoxOne [deviceState=").append(deviceState).append(", bright=").append(bright).append("]");
        return builder.toString();
    }

}
