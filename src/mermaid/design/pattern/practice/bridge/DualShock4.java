package mermaid.design.pattern.practice.bridge;

public class DualShock4 extends Controller {

    public DualShock4(HomeConsole homeConsole) {
        super(homeConsole);
        System.out.println("Use DualShock4 now...");
    }

    @Override
    public void pressButtonStore() {
        this.homeConsole.connectToStore();
    }
    
    public void pressButtonUp() {
        this.homeConsole.turnScreenBrigter();
    }
    
    public void pressButtonDown() {
        this.homeConsole.turnScreenDarker();
    }

}
