package mermaid.design.pattern.practice.bridge;

import java.util.Arrays;
import java.util.List;

public class PlayStation4 extends HomeConsole {
    private List<String> listOfGame;
    
    public PlayStation4(int bright) {
        this.bright = bright;
        this.deviceState = DeviceState.POWEROFF;
        this.listOfGame = Arrays.asList(
            new String[] {"Monster Hunter", "Demon May Cry", "Street Fighter5", "Dark Soul", "Over Cooked 2"}
        );
        System.out.println("Connect to PS4 now...");
    }
    
    public void connectToStore() {
        System.out.println("Games in PS Store we have below:");
        this.listOfGame.stream().forEach(System.out::println);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("PlayStation4[deviceState=").append(deviceState).append(", bright=").append(bright).append("]");
        
        return builder.toString();
    }
    
}
