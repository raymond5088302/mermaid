package mermaid.design.pattern.practice.bridge;

/* 
 * refer to https://www.youtube.com/watch?v=jfVCn8qJ690
 * refer to https://www.youtube.com/watch?v=XuTwtOo88r8
 * refer to https://www.youtube.com/watch?v=uQXN3Azvq9E
 * refer to https://www.youtube.com/watch?v=uKTaZUD72jU
 */
public class Main {

    /* 
     * The relationship between two sides that I temporarily call them "A" and "B".
     * "A" side is like client has Controller to be abstraction, then DualShock4 and XBoxController to be its concrete subclass.
     * "B" side is like remote that has HomeConsole to be abstraction, then PlayStation4 and XBoxOne to be its concrete subclass.
     * Whatever the construction of two sides has changed, they don't affect each other. 
     * So the one advantage of using this pattern gets lower dependences and can increase complexity independently.
     * */
    public static void main(String[] args) {
        PlayStation4 ps4Pro = new PlayStation4(5);
        DualShock4 dualShock4 = new DualShock4(ps4Pro);
        dualShock4.pressButtonStart();
        dualShock4.pressButtonStore();
        dualShock4.pressButtonDown();
        dualShock4.pressButtonDown();
        dualShock4.pressButtonUp();
        System.out.println(ps4Pro);
        System.out.println();
        
        XBoxOne xboxOne = new XBoxOne(10);
        XBoxController xboxFirstController = new XBoxController(xboxOne);
        xboxFirstController.pressButtonStart();
        xboxFirstController.pressButtonStore();
        xboxFirstController.pressbuttonRight();
        xboxFirstController.pressbuttonRight();
        xboxFirstController.pressButtonLeft();
        xboxFirstController.pressButtonShutdown();
        System.out.println(xboxOne);
        System.out.println();
        
        PlayStation4 ps4Slim = new PlayStation4(7);
        XBoxController xboxSecondController = new XBoxController(ps4Slim);
        xboxSecondController.pressButtonStart();
        xboxSecondController.pressButtonStore();
    }

}
