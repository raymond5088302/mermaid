package mermaid.design.pattern.practice.bridge;

import java.util.List;


public class XBoxController extends Controller {

    
    public XBoxController(HomeConsole homeConsole) {
        super(homeConsole);
        System.out.println("Use XBox Controller now...");
    }

    @Override
    public void pressButtonStore() {
        this.homeConsole.connectToStore();
    }
    
    public void pressbuttonRight() {
        this.homeConsole.turnScreenBrigter();
    }
    
    public void pressButtonLeft() {
        this.homeConsole.turnScreenDarker();
    }

}
