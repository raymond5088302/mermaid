package mermaid.design.pattern.practice.prototype3;

import java.util.Optional;

public interface Fruit extends Cloneable {
    public Optional<? super Fruit> copycat();
}
