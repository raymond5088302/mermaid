package mermaid.design.pattern.practice.prototype3;

import java.util.Optional;

public class Clonerizer {
    public static Optional<? super Fruit> getClone(Fruit prototype) {
        return prototype.copycat();
    }
}
