package mermaid.design.pattern.practice.prototype3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/* idea from https://www.youtube.com/watch?v=jvnKybI7fzU */
public class Main {

    public static void main(String[] args) {
        Orange originalAnnoyingOrange = new Orange(Arrays.asList(
            new Seed(3, "Red"),
            new Seed(8, "Blue"),
            new Seed(10, "Yellow")
        ));
        System.out.printf("Original annoying orange: %s%n", originalAnnoyingOrange);
        
        List<Orange> oranges = Stream.generate(
            () -> (Orange) Clonerizer.getClone(originalAnnoyingOrange).orElseThrow()
        )
        .limit(3)
        .collect(Collectors.toCollection(ArrayList::new));
        oranges.get(1).getSeeds().get(1).setColor("Green");
        oranges.get(1).getSeeds().get(2).setSize(999);
        oranges.forEach(
            clone -> {
                System.out.printf("Annoying orange's clone: %s%n", clone);
            }
        );
        
        Apple originalAngryApple = new Apple(Arrays.asList(
            new Seed(101, "white"),
            new Seed(103, "rose"),
            new Seed(105, "pink")
        ));
        System.out.printf("%nOriginal angry apple: %s%n", originalAngryApple);
        List<Apple> apples = Stream.generate(
            () -> (Apple) Clonerizer.getClone(originalAngryApple).orElseThrow()
        )
        .limit(3)
        .collect(Collectors.toCollection(ArrayList::new));
        apples.get(1).getSeeds().add(new Seed(25, "Yellow"));
        apples.forEach(
            clone -> {
                System.out.printf("angry apple's clone: %s%n", clone);
            }
        );
    }
    
}
