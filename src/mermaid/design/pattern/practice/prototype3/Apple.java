package mermaid.design.pattern.practice.prototype3;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Apple implements Fruit{
    private List<Seed> seeds;
    
    public Apple(List<Seed> seeds) {
        this.seeds = seeds;
    }
    
    @Override
    public Optional<? super Fruit> copycat() {
        Apple clone = null;
        
        try {
            clone = (Apple) super.clone();
            clone.seeds = this.seeds.stream()
                .map(seed -> (Seed) seed.clone())
                .collect(Collectors.toCollection(ArrayList::new));
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        
        return Optional.ofNullable(clone);
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Apple [hashcode=")
            .append(System.identityHashCode(this))
            .append(", seeds=")
            .append(seeds)
            .append("");
        
        return builder.toString();
    }
    
    public List<Seed> getSeeds() {
        return seeds;
    }
    
    public void setSeeds(List<Seed> seeds) {
        this.seeds = seeds;
    }
}

