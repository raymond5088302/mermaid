package mermaid.design.pattern.practice.prototype3;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Orange implements Fruit {
    private List<Seed> seeds;
    
    public Orange(List<Seed> seeds) {
        this.seeds = seeds;
    }

    @Override
    public Optional<? super Fruit> copycat() {
        Orange clone = null;
        
        try {
            clone = (Orange) super.clone();
            clone.seeds = this.seeds.stream()
                .map(seed -> (Seed) seed.clone())
                .collect(Collectors.toCollection(ArrayList::new));
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        
        return Optional.ofNullable(clone);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Orange [hashcode=")
            .append(System.identityHashCode(this))
            .append(", seeds=")
            .append(seeds)
            .append("");
        
        return builder.toString();
    }

    public List<Seed> getSeeds() {
        return seeds;
    }

    public void setSeeds(List<Seed> seeds) {
        this.seeds = seeds;
    }
}
