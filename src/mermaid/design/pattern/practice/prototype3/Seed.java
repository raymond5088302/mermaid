package mermaid.design.pattern.practice.prototype3;

public class Seed implements Cloneable {
    private int size;
    private String color;
    
    public Seed(int size, String color) {
        this.size = size;
        this.color = color;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    
    public Object clone() {
        Object clone = null;
        
        try {
            clone = super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return clone;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("(");
        builder.append(size);
        builder.append(", ");
        builder.append(color);
        builder.append(", ");
        builder.append(System.identityHashCode(this));
        builder.append(")");
        return builder.toString();
    }
}
