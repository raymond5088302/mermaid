package mermaid.design.pattern.practice.strategy1;

public interface IAbilityToTongueOut {
    String tongueOut();
}
