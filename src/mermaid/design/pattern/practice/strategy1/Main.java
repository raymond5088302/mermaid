package mermaid.design.pattern.practice.strategy1;

/* Reference to https://www.youtube.com/watch?v=DD5UKQggXTc */
public class Main {

    public static void main(String[] args) {
       AnnoyingOrange annoyingOrange = new AnnoyingOrange("AnnoyingOrange", 250, 180, "Hey!!", false);
       AngryApple angryApple = new AngryApple("AngryApple", 280, 90, "What is it!!", false);
       
       annoyingOrange.annoy(angryApple);
       angryApple.angry(annoyingOrange);
       annoyingOrange.setAbilityToTongueOut(new FruitCanTongueOut());
       angryApple.setAbilityToTongueOut(new FruitCantTongueOut());
       annoyingOrange.showMeTongueOut();
       angryApple.showMeTongueOut();
    }

}
