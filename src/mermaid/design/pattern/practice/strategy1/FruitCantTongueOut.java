package mermaid.design.pattern.practice.strategy1;

public class FruitCantTongueOut implements IAbilityToTongueOut{

    @Override
    public String tongueOut() {
        return "can not tongue out";
    }

}
