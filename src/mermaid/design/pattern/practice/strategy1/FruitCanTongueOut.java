package mermaid.design.pattern.practice.strategy1;

public class FruitCanTongueOut implements IAbilityToTongueOut {

    @Override
    public String tongueOut() {
        return "can tongue out";
    }

}
