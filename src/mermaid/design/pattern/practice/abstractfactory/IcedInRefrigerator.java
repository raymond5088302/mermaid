package mermaid.design.pattern.practice.abstractfactory;

public class IcedInRefrigerator implements IWayToIceCarton {
    
    public String getWayToIceCarton() {
        return "to put in the refrigerator";
    }
    
}
