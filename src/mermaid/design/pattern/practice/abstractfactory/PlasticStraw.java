package mermaid.design.pattern.practice.abstractfactory;

public class PlasticStraw implements IMaterialOfStraw {
    
    public String getMaterialOfStraw() {
        return "plastic";
    }

}
