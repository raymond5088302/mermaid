package mermaid.design.pattern.practice.abstractfactory;

public class RaspberryJuiceCarton extends JuiceCarton {
    protected boolean hasBecomeRage; 
    
    public RaspberryJuiceCarton(String name, int capacity, double temperature, boolean hasBecomeRage) {
        super(name, capacity, temperature);
        this.hasBecomeRage = hasBecomeRage;
    }
    
    public void showRage() {
        if (hasBecomeRage) {
            System.out.printf("%s is becoming rage now!!", super.name);            
        } else {
            System.out.printf("%s keep quiet!!", super.name);  
        }
    }
    
}
