package mermaid.design.pattern.practice.abstractfactory;

public class IronStraw implements IMaterialOfStraw {
    
    public String getMaterialOfStraw() {
        return "Icon";
    }

}
