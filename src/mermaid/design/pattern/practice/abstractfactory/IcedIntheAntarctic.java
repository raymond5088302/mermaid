package mermaid.design.pattern.practice.abstractfactory;

public class IcedIntheAntarctic implements IWayToIceCarton {
    
    public String getWayToIceCarton() {
        return "put items in Antarctic";
    }
    
}
