package mermaid.design.pattern.practice.abstractfactory;

import java.util.ArrayList;
import java.util.Map;

public class SevenEleventConvenienceStore extends ConvenienceStore {

    public SevenEleventConvenienceStore(Map<JuiceFactory.Flavor, Integer> cartonsInStock) {
        super(cartonsInStock);
    }

    @Override
    public ArrayList<JuiceCarton> orderCarton() {
        ArrayList<JuiceCarton> cartons = new ArrayList<JuiceCarton>(); 
        Map<JuiceFactory.Flavor, Integer> needsForCarton = carryStockChecks();
        JuiceFactory factory = new SevenEleventJuiceFactory();
        needsForCarton.forEach(
            (flavor, count) -> {
                for (int i = 0; i < count; i++) {
                    cartons.add(factory.produceJuiceCarton(flavor));
                }
            }
        );
        return cartons;
    }
    
}
