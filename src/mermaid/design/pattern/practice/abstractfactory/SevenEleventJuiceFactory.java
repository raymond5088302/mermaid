package mermaid.design.pattern.practice.abstractfactory;

public class SevenEleventJuiceFactory extends JuiceFactory {
    
    public JuiceCarton produceJuiceCarton(JuiceFactory.Flavor flavor) {
        JuiceCarton carton = super.produceJuiceCarton(flavor);
        carton.setMaterialOfStraw(addStraw());
        carton.setWayToIce(addWayToIced());

        return carton;
    }

    public IMaterialOfStraw addStraw() {

        return new IronStraw();
    }

    public IcedInRefrigerator addWayToIced() {

        return new IcedInRefrigerator();
    }

}
