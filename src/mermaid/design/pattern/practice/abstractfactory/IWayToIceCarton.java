package mermaid.design.pattern.practice.abstractfactory;

public interface IWayToIceCarton {
    
    String getWayToIceCarton();
    
}
