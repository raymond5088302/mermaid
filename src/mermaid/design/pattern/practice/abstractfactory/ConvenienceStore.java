package mermaid.design.pattern.practice.abstractfactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public abstract class ConvenienceStore {
    Map<JuiceFactory.Flavor, Integer> cartonsInStock;
    
    public ConvenienceStore(Map<JuiceFactory.Flavor, Integer> cartonsInStock) {
        this.cartonsInStock = cartonsInStock;
    }
    
    public Map<JuiceFactory.Flavor, Integer> carryStockChecks() {
        Map<JuiceFactory.Flavor, Integer> needsForCarton = new HashMap<JuiceFactory.Flavor, Integer>();
        this.cartonsInStock.forEach(
            (favor, count) -> {
                if (count < 20) {
                    needsForCarton.put(favor, 20 - count);
                }
            }
        );
        return needsForCarton;
    }
    
    public abstract ArrayList<JuiceCarton> orderCarton();

}
