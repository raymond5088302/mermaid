package mermaid.design.pattern.practice.abstractfactory;

public class OrangeJuiceCarton extends JuiceCarton {
    protected boolean hasBecomeAnnoying;
    
    public OrangeJuiceCarton(String name, int capacity, double temperature, boolean hasBecomeAnnoying) {
        super(name, capacity, temperature);
        this.hasBecomeAnnoying = hasBecomeAnnoying;
    }
    
    public void startAnnoying() {
        if (hasBecomeAnnoying) {
            System.out.printf("%s is becoming annoying now!!", super.name);            
        } else {
            System.out.printf("%s keep quiet!!", super.name);   
        }
    }
    
}
