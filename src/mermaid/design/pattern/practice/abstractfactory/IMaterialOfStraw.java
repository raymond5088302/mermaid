package mermaid.design.pattern.practice.abstractfactory;

public interface IMaterialOfStraw {
    
    String getMaterialOfStraw();

}
