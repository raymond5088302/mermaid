package mermaid.design.pattern.practice.abstractfactory;

public class FamilyMartJuiceFactory extends JuiceFactory {
    
   public JuiceCarton produceJuiceCarton(JuiceFactory.Flavor flavor) {
       JuiceCarton carton = super.produceJuiceCarton(flavor);
       carton.setMaterialOfStraw(addStraw());
       carton.setWayToIce(addWayToIced());
       
       return carton;
   }
    
   public IMaterialOfStraw addStraw() {
       
       return new PlasticStraw();
   }
   
   public IWayToIceCarton addWayToIced() {
       
       return new IcedIntheAntarctic();
   }
   
}
