package mermaid.design.pattern.practice.abstractfactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* Reference to https://www.youtube.com/watch?v=MkCyVId3KY8 */
public class Main {
    public static void main(String[] args) {
        
        /*
         * There are two different convenience stores that have same product (three types of juice cartons),
         * but sections of the product are not same. For example like the straw is made of iron or plastic or various ways
         * to ice carton, etc. Their owned factories will determine how to produce and design their products.
         */
        Map<JuiceFactory.Flavor, Integer> cartonsOfSevenEleventInStock = new HashMap<JuiceFactory.Flavor, Integer>();
        
        /* The convenience store employees will fill products when the number is below 20. */
        cartonsOfSevenEleventInStock.put(JuiceFactory.Flavor.CHERRY, Integer.valueOf(17));
        cartonsOfSevenEleventInStock.put(JuiceFactory.Flavor.ORANGE, Integer.valueOf(25));
        cartonsOfSevenEleventInStock.put(JuiceFactory.Flavor.RASPBERRY, Integer.valueOf(18));
        ConvenienceStore sevenEleventConvenienceStore = new SevenEleventConvenienceStore(cartonsOfSevenEleventInStock);
        ArrayList<JuiceCarton> sevenEleventCartons = sevenEleventConvenienceStore.orderCarton();
        System.out.println("7-11 convenience store will refill proeucts below:");
        for (JuiceCarton carton : sevenEleventCartons) {
            System.out.println(carton.toString());
        }
        System.out.println();
        Map<JuiceFactory.Flavor, Integer> cartonsOfFamilyMartInStock = new HashMap<JuiceFactory.Flavor, Integer>();
        cartonsOfFamilyMartInStock.put(JuiceFactory.Flavor.CHERRY, Integer.valueOf(19));
        cartonsOfFamilyMartInStock.put(JuiceFactory.Flavor.ORANGE, Integer.valueOf(17));
        cartonsOfFamilyMartInStock.put(JuiceFactory.Flavor.RASPBERRY, Integer.valueOf(18));
        ConvenienceStore familyMartConvenienceStore = new FamilyMartConvenienceStore(cartonsOfFamilyMartInStock);
        ArrayList<JuiceCarton> familyMartCartons = familyMartConvenienceStore.orderCarton();
        System.out.println("Family Mart convenience store will refill proeucts below:");
        for (JuiceCarton carton : familyMartCartons) {
            System.out.println(carton.toString());
        }
    }
    
}
