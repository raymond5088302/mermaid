package mermaid.design.pattern.practice.command;

public class MoveBackward implements Command{
    
    private Hunter hunter;
    
    public MoveBackward(Hunter hunter) {
        this.hunter = hunter;
    }

    @Override
    public void execute() {
        this.hunter.moveBackward();
    }

    @Override
    public void undo() {
        this.hunter.moveForward();
    }

}
