package mermaid.design.pattern.practice.command;

import java.util.HashMap;
import java.util.Map;

/* refer to https://www.youtube.com/watch?v=OltfWqxcGUs
 * refer to https://www.youtube.com/watch?v=g3Alvh9yUuM
 */
public class Main {

    public static void main(String[] args) {
        ChargeBlade chargeBladeHunter = new ChargeBlade();
        Map<DualShock4.Button, Command> chargeBladeCommands = (new HashMap<DualShock4.Button, Command>());
        chargeBladeCommands.put(DualShock4.Button.TRIANGLE, new RaiseWeapon(chargeBladeHunter));
        chargeBladeCommands.put(DualShock4.Button.CIRCLE, new TakeBackWeapon(chargeBladeHunter));
        chargeBladeCommands.put(DualShock4.Button.CROSS, new MoveForward(chargeBladeHunter));
        chargeBladeCommands.put(DualShock4.Button.SQUARE, new MoveBackward(chargeBladeHunter));
        DualShock4 chargeBladeController = new DualShock4(chargeBladeCommands);
        chargeBladeController.press(DualShock4.Button.TRIANGLE);
        chargeBladeController.press(DualShock4.Button.CROSS);
        System.out.println(chargeBladeHunter);
        chargeBladeController.undo(DualShock4.Button.TRIANGLE);
        chargeBladeController.undo(DualShock4.Button.CROSS);
        System.out.println(chargeBladeHunter);
        System.out.println();
        
        Bow bowHunter = new Bow();
        Map<DualShock4.Button, Command> bowCommands = (new HashMap<DualShock4.Button, Command>());
        bowCommands.put(DualShock4.Button.TRIANGLE, new MoveForward(bowHunter));
        bowCommands.put(DualShock4.Button.CIRCLE, new MoveRight(bowHunter));
        bowCommands.put(DualShock4.Button.CROSS, new RaiseWeapon(bowHunter));
        bowCommands.put(DualShock4.Button.SQUARE, new TakeBackWeapon(bowHunter));
        DualShock4 bowController = new DualShock4(bowCommands);
        bowController.press(DualShock4.Button.CIRCLE);
        bowController.press(DualShock4.Button.CROSS);
        System.out.println(bowHunter);
        bowController.undo(DualShock4.Button.CIRCLE);
        bowController.undo(DualShock4.Button.CROSS);
        System.out.println(bowHunter);
    }

}
