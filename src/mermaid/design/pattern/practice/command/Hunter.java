package mermaid.design.pattern.practice.command;

public interface Hunter {
    public void raiseWeapon();
    public void takeBackWeapon();
    public void moveForward();
    public void moveBackward();
    public void moveRight();
    public void moveLeft();
}
