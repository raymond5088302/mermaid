package mermaid.design.pattern.practice.command;

public class MoveLeft implements Command{
    
    private Hunter hunter;
    
    public MoveLeft(Hunter hunter) {
        this.hunter = hunter;
    }

    @Override
    public void execute() {
        this.hunter.moveLeft();
    }

    @Override
    public void undo() {
        this.hunter.moveRight();
    }

}
