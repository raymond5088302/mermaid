package mermaid.design.pattern.practice.command;

import java.awt.Point;

/* I'm receiver */
public class ChargeBlade implements Hunter {
    public int x;
    public int y;
    public boolean readyToFight;
    
    public ChargeBlade() {
        this.x = 10;
        this.y = 10;
        this.readyToFight = false;
    }
    
    @Override
    public void raiseWeapon() {
        System.out.println("The hunter raises his sword!");
        this.readyToFight = true;
    }
    @Override
    public void takeBackWeapon() {
        System.out.println("The hunter takes back his sword!");
        this.readyToFight = false;
    }
    @Override
    public void moveForward() {
        System.out.println("The hunter moves forward!");
        this.y++;
    }
    @Override
    public void moveBackward() {
        System.out.println("The hunter moves backward!");
        this.y--;
    }
    @Override
    public void moveRight() {
        System.out.println("The hunter moves right!");
        this.x++;
    }
    @Override
    public void moveLeft() {
        System.out.println("The hunter moves left!");
        this.x--;
    }
    
    @Override
    public String toString() {
        return "ChargeBlade [x=" + x + ", y=" + y + ", readyToFight=" + readyToFight + "]";
    }
    
}
