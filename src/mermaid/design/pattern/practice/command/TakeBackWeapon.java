package mermaid.design.pattern.practice.command;

public class TakeBackWeapon implements Command{
    
    private Hunter hunter;
    
    public TakeBackWeapon(Hunter hunter) {
        this.hunter = hunter;
    }

    @Override
    public void execute() {
        this.hunter.takeBackWeapon();
    }

    @Override
    public void undo() {
        this.hunter.raiseWeapon();
    }

}
