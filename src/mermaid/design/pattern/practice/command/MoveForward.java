package mermaid.design.pattern.practice.command;

public class MoveForward implements Command{
    
    private Hunter hunter;
    
    public MoveForward(Hunter hunter) {
        this.hunter = hunter;
    }

    @Override
    public void execute() {
        this.hunter.moveForward();
    }

    @Override
    public void undo() {
        this.hunter.moveBackward();
    }

}
