package mermaid.design.pattern.practice.command;

public interface Command {
    public void execute();
    public void undo();
}
