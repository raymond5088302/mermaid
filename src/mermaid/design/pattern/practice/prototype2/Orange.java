package mermaid.design.pattern.practice.prototype2;

import java.util.Optional;

public class Orange implements Fruit {
    private int weight;
    
    public Orange(int weight) {
        this.weight = weight;
    }

    @Override
    public Optional<? super Fruit> copycat() {
        Optional<? super Fruit> clone = Optional.empty();
        
        try {
            clone = Optional.of((Orange) super.clone());
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        
        return clone;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Orange [weight=");
        builder.append(weight);
        builder.append("]");
        
        return builder.toString();
    }

}
