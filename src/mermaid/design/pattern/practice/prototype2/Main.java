package mermaid.design.pattern.practice.prototype2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/* idea from https://www.youtube.com/watch?v=jvnKybI7fzU */
public class Main {

    public static void main(String[] args) {
        Fruit originalAnnoyingOrange = new Orange(30);
        System.out.printf("The content of original annoying orange: %s%n", originalAnnoyingOrange);
        System.out.printf("The hashcode of original annoying orange: %d%n", System.identityHashCode(originalAnnoyingOrange));
        Stream.generate(
            () -> (Fruit) Clonerizer.getClone(originalAnnoyingOrange).orElseThrow()
        )
        .limit(5)
        .collect(Collectors.toCollection(ArrayList::new))
        .forEach(
            clone -> {
                System.out.printf("The content of annoying orange's clone: %s%n", clone);
                System.out.printf("The hashcode of annoying orange's clone: %d%n", System.identityHashCode(clone));
            }
        );
        
        Fruit originalAngryApple = new Apple(8);
        System.out.printf("%nThe content of original angry apple: %s%n", originalAngryApple);
        System.out.printf("The hashcode of original angry apple: %d%n", System.identityHashCode(originalAngryApple));
        Stream.generate(
            () -> (Fruit) Clonerizer.getClone(originalAngryApple).orElseThrow()
        )
        .limit(5)
        .collect(Collectors.toCollection(ArrayList::new))
        .forEach(
            clone -> {
                System.out.printf("The content of angry apple's clone: %s%n", clone);
                System.out.printf("The hashcode of angry apple's clone: %d%n", System.identityHashCode(clone));
            }
        );      
    }
    
}
