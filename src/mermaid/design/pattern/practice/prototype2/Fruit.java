package mermaid.design.pattern.practice.prototype2;

import java.util.Optional;

public interface Fruit extends Cloneable {
    public Optional<? super Fruit> copycat();
}
