package mermaid.design.pattern.practice.prototype2;

import java.util.Optional;

public class Apple implements Fruit {
    private int sweet;
    
    public Apple(int sweet) {
        this.sweet = sweet;
    }

    @Override
    public Optional<? super Fruit> copycat() {
        Optional<? super Fruit> clone = Optional.empty();
        
        try {
            clone = Optional.of((Apple) super.clone());
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        
        return clone;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Apple [sweet=");
        builder.append(sweet);
        builder.append("]");
        return builder.toString();
    }

}
