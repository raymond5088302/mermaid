package mermaid.design.pattern.practice.facade;

public class Boller {
    private enum Fluid {
        WATER, MILK
    }
    private int capacity = 0;
    private int temperature = 0;
    private Fluid liquid;
    
    public Boller() {}
    
    public int heat(int capacity, int temperature) {
        System.out.println("Boller start heating water!");
        this.capacity = capacity;
        this.temperature = temperature;
        this.liquid = Fluid.WATER;
        return (int) (this.capacity * (this.temperature - 25) * 0.001);
    }
    
    public int steam(int capacity) {
        System.out.println("Boller start steaming milk!");
        this.capacity = capacity;
        this.temperature = 100;
        this.liquid = Fluid.MILK;
        return (int) (this.capacity * (100 - 25) * 0.005);
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public Fluid getLiquid() {
        return liquid;
    }

    public void setLiquid(Fluid liquid) {
        this.liquid = liquid;
    }

}
