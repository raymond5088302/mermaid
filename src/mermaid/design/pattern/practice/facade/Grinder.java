package mermaid.design.pattern.practice.facade;

public class Grinder {
    public enum POWERSIZE{
        SMALL, MEDIUM, LARGE
    };
    private POWERSIZE size;
    private int weight;
    
    public Grinder() {
    }
    
    public int grind(POWERSIZE size, int weight) {
        this.size = size;
        this.weight = weight;
        double secPerGram = 0.0;
        switch(size) {
            case SMALL:
                secPerGram = 0.015;
                break;
            case MEDIUM:
                secPerGram = 0.012;
                break;
            case LARGE:
                secPerGram = 0.015;
                break;
        }
        System.out.println("Grinder start grinding coffee beans!");
        return (int)(this.weight * secPerGram);
    }
    
    public POWERSIZE getSize() {
        return size;
    }

    public void setSize(POWERSIZE size) {
        this.size = size;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
