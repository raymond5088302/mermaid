package mermaid.design.pattern.practice.facade;

import mermaid.design.pattern.practice.facade.AutomaticCoffeemaker.CoffeeCategory;

/* idea from https://www.youtube.com/watch?v=RkSQj3h2F6A */
public class Main {
    public static void main(String[] args) {
        AutomaticCoffeemaker automaticCoffeemaker = new AutomaticCoffeemaker();
        System.out.println(automaticCoffeemaker.brew(CoffeeCategory.ESPRESSO, 2));
        System.out.println("");
        System.out.println(automaticCoffeemaker.brew(CoffeeCategory.AMERICANO, 3));
        System.out.println("");
        System.out.println(automaticCoffeemaker.brew(CoffeeCategory.LATTEE, 5));
    }
}
