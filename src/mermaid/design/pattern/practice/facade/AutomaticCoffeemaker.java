package mermaid.design.pattern.practice.facade;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import mermaid.design.pattern.practice.facade.FlavorMaker.Spice;

public class AutomaticCoffeemaker {
    public enum CoffeeCategory {
        ESPRESSO, AMERICANO, LATTEE
    }
    Boller boller;
    Grinder grinder;
    FlavorMaker flavorMaker;
    
    public AutomaticCoffeemaker() {
        this.boller = new Boller();
        this.grinder = new Grinder();
        this.flavorMaker = new FlavorMaker();
    }
    
    public String brew(CoffeeCategory coffeeCategory, int totalCups) {
        String format = "%s cups of %s[Spending time: %d sec, coffee bean weight: %d grams, powder size: %s, Fluid: %s, capacity: %d cc, tempature: %d degrees, spices: %s]";
        String result = null;
        int spendingTime = 0;
        List<Spice> spices = new ArrayList<>();
        
        switch(coffeeCategory) {
            case ESPRESSO:
                spendingTime = this.grinder.grind(Grinder.POWERSIZE.SMALL, totalCups * 12);
                spendingTime += this.boller.heat(30 * totalCups, 90);
                spices = this.flavorMaker.flavor(FlavorMaker.Flavor.NORMAL);
                break;
            case AMERICANO:
                spendingTime = this.grinder.grind(Grinder.POWERSIZE.LARGE, totalCups * 20);
                spendingTime += this.boller.heat(250 * totalCups, 90);
                spices = this.flavorMaker.flavor(FlavorMaker.Flavor.NORMAL);
                break;
            case LATTEE:
                spendingTime = this.grinder.grind(Grinder.POWERSIZE.MEDIUM, totalCups * 15);
                spendingTime += this.boller.steam(300 * totalCups);
                spices = this.flavorMaker.flavor(FlavorMaker.Flavor.SPECIAL);
                break;
        }
        
        result = String.format(
            format,
            totalCups,
            coffeeCategory,
            spendingTime,
            this.grinder.getWeight(),
            this.grinder.getSize(),
            this.boller.getLiquid(), 
            this.boller.getCapacity(), 
            this.boller.getTemperature(),
            spices
        );
        
        return result;
    }
    
    
}
