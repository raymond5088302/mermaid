package mermaid.design.pattern.practice.facade;

import java.util.ArrayList;
import java.util.List;

public class FlavorMaker {
    public enum Spice {
        SUGAR, GINGER, CINNAMON
    }
    public enum Flavor {
        NORMAL, SPECIAL
    }
    
    public FlavorMaker() {
    }
    
    public List<Spice> flavor(Flavor flavor) {
        List<Spice> spices = new ArrayList<>();
        
        System.out.println("FlavorMaker start adding spices!");
        switch (flavor) {
            case NORMAL:
                spices.add(Spice.SUGAR);
            break;
            case SPECIAL:
                spices.add(Spice.GINGER);
                spices.add(Spice.CINNAMON);
                break;
        }
        
        return spices;
    }
}
