package mermaid.design.pattern.practice.adapter;

public interface XBoxController {
    public void buttonY();
    public void buttonB();
    public void buttonA();
    public void buttonX();
}
