package mermaid.design.pattern.practice.adapter;

public interface Command {
    public void execute();
    public void undo();
}
