package mermaid.design.pattern.practice.adapter;

import java.awt.Point;

/* I'm receiver */
public class Bow implements Hunter {
    public int x;
    public int y;
    public boolean readyToFight;
    
    public Bow() {
        this.x = 10;
        this.y = 10;
        this.readyToFight = false;
    }
    
    @Override
    public void raiseWeapon() {
        System.out.println("The hunter raises his bow!");
        this.readyToFight = true;
    }
    @Override
    public void takeBackWeapon() {
        System.out.println("The hunter takes back his bow!");
        this.readyToFight = false;
    }
    @Override
    public void moveForward() {
        System.out.println("The hunter slides forward!");
        this.y+=5;
    }
    @Override
    public void moveBackward() {
        System.out.println("The hunter slides backward!");
        this.y-=5;
    }
    @Override
    public void moveRight() {
        System.out.println("The hunter slides right!");
        this.x+=5;
    }
    @Override
    public void moveLeft() {
        System.out.println("The hunter slides left!");
        this.x-=5;
    }
    
    @Override
    public String toString() {
        return "Bow [x=" + x + ", y=" + y + ", readyToFight=" + readyToFight + "]";
    }
    
}
