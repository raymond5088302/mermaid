package mermaid.design.pattern.practice.adapter;

import java.util.HashMap;
import java.util.Map;

/* 
 * refer to https://www.youtube.com/watch?v=uQXN3Azvq9E
 * refer to https://www.youtube.com/watch?v=uKTaZUD72jU
 */
public class Main {

    public static void main(String[] args) {
        ChargeBlade chargeBladeHunter = new ChargeBlade();
        Map<DualShock4.Button, Command> chargeBladeCommands = (new HashMap<DualShock4.Button, Command>());
        chargeBladeCommands.put(DualShock4.Button.TRIANGLE, new RaiseWeapon(chargeBladeHunter));
        chargeBladeCommands.put(DualShock4.Button.CIRCLE, new TakeBackWeapon(chargeBladeHunter));
        chargeBladeCommands.put(DualShock4.Button.CROSS, new MoveForward(chargeBladeHunter));
        chargeBladeCommands.put(DualShock4.Button.SQUARE, new MoveBackward(chargeBladeHunter));
        DualShock4 dualShock4 = new DualShock4(chargeBladeCommands);
        dualShock4.press(DualShock4.Button.TRIANGLE);
        dualShock4.press(DualShock4.Button.CIRCLE);
        dualShock4.press(DualShock4.Button.CROSS);
        dualShock4.press(DualShock4.Button.CROSS);
        System.out.println(chargeBladeHunter);
        System.out.println();
        
        System.out.println("I change my controller from DualShock4 to XBoxOneController");
        XBoxControllerAdapter XBoxOneController = new XBoxControllerAdapter(dualShock4);
        XBoxOneController.buttonY();
        XBoxOneController.buttonB();
        XBoxOneController.buttonY();
        XBoxOneController.buttonA();
        XBoxOneController.buttonA();
        XBoxOneController.buttonX();
        System.out.println(chargeBladeHunter);
    }

}
