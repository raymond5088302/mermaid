package mermaid.design.pattern.practice.adapter;

public class MoveRight implements Command{
    
    private Hunter hunter;
    
    public MoveRight(Hunter hunter) {
        this.hunter = hunter;
    }

    @Override
    public void execute() {
        this.hunter.moveRight();
    }

    @Override
    public void undo() {
        this.hunter.moveLeft();
    }

}
