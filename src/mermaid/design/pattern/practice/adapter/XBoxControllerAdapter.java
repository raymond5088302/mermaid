package mermaid.design.pattern.practice.adapter;

import mermaid.design.pattern.practice.adapter.DualShock4.Button;

public class XBoxControllerAdapter implements XBoxController {
    DualShock4 dualShock4;
    
    public XBoxControllerAdapter(DualShock4 dualShock4) {
        this.dualShock4 = dualShock4;
    }
    
    @Override
    public void buttonY() {
        dualShock4.press(Button.TRIANGLE);
    }

    @Override
    public void buttonB() {
        dualShock4.press(Button.CIRCLE);
    }

    @Override
    public void buttonA() {
        dualShock4.press(Button.CROSS);
    }

    @Override
    public void buttonX() {
        dualShock4.press(Button.SQUARE);
    }

}
