package mermaid.design.pattern.practice.adapter;

import java.util.Map;

/* I'm invoker */
public class DualShock4 {
    public enum Button {
        TRIANGLE, CIRCLE, CROSS, SQUARE
    }
    private Map<Button, Command> commands;
    
    public DualShock4(Map<Button, Command> commands) {
        this.commands = commands;
    }
    
    public void press(Button button) {
        this.commands.get(button).execute();
    }
    
    public void undo(Button button) {
        this.commands.get(button).undo();
    }

}
