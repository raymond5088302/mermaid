package mermaid.design.pattern.practice.adapter;

public class RaiseWeapon implements Command{
    
    private Hunter hunter;
    
    public RaiseWeapon(Hunter hunter) {
        this.hunter = hunter;
    }

    @Override
    public void execute() {
        this.hunter.raiseWeapon();
    }

    @Override
    public void undo() {
        this.hunter.takeBackWeapon();
    }

}
