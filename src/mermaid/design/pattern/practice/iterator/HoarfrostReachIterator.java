package mermaid.design.pattern.practice.iterator;

import java.util.Iterator;
import java.util.Map;

public class HoarfrostReachIterator implements Iterator<Material> {
    private Map<Integer, Material> mapOfMaterials;
    private Integer index;
    
    public HoarfrostReachIterator(Map<Integer, Material> mapOfMaterials) {
        super();
        this.mapOfMaterials = mapOfMaterials;
        this.index = 0;
    }

    @Override
    public boolean hasNext() {
        return (mapOfMaterials != null)  && !mapOfMaterials.isEmpty() && (this.mapOfMaterials.containsKey(index));
    }

    @Override
    public Material next() {
        Material material = mapOfMaterials.get(index);
        index++;
        return material;
    }
}
