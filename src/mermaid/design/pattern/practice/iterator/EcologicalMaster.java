package mermaid.design.pattern.practice.iterator;

import java.util.Iterator;

public class EcologicalMaster {
    AncientForest ancientForest;
    HoarfrostReach hoarfrostReach;
    
    public EcologicalMaster(AncientForest ancientForest, HoarfrostReach hoarfrostReach) {
        this.ancientForest = ancientForest;
        this.hoarfrostReach = hoarfrostReach;
    }
    
    public void printAllMaterial() {
        Iterator<Material> ancientForestIterator = this.ancientForest.createIterator();
        Iterator<Material> hoarfrostReachIterator = this.hoarfrostReach.createIterator();
        
        System.out.println("Show all materials of Ancient Forest below:");
        while (ancientForestIterator.hasNext()) {
            System.out.println(ancientForestIterator.next().toString());
        }
        System.out.println();
        
        System.out.println("Show all materials of Hoarfrost Reach below:");
        while (hoarfrostReachIterator.hasNext()) {
            System.out.println(hoarfrostReachIterator.next().toString());
        }
    }
}