package mermaid.design.pattern.practice.iterator;

public class Material {
    private String name;
    private int space;
    private String symbol;
    
    public Material(String name, int space, String symbol) {
        super();
        this.name = name;
        this.space = space;
        this.symbol = symbol;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Material [name=").append(name).append(", space=").append(space).append(", symbol=")
            .append(symbol).append("]");
        return builder.toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSpace() {
        return space;
    }

    public void setSpace(int space) {
        this.space = space;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
}
