package mermaid.design.pattern.practice.iterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AncientForest{
    List<Material> listOfMaterials;

    public AncientForest() {
        this.listOfMaterials = new ArrayList<Material>();
        addMaterial("Herb", 1, "H");
        addMaterial("Ancient Bone", 2, "B");
        addMaterial("Iron Ore", 1, "#");
    }
    
    public void addMaterial(String name, int space, String symbol) {
        this.listOfMaterials.add(new Material(name, space, symbol));
    }
    
    public Iterator<Material> createIterator() {
        return new AncientForestIterator(this.listOfMaterials);
    }
}