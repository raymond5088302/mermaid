package mermaid.design.pattern.practice.iterator;

/*
 * Ancient Forest: https://www.youtube.com/watch?v=zNfK-YqLUbU
 * Hoarfrost Reach: https://www.youtube.com/watch?v=RhGGBhZvQOc
 * */
public class Main {

    public static void main(String[] args) {
        EcologicalMaster ecologicalMaster = new EcologicalMaster(new AncientForest(), new HoarfrostReach());
        ecologicalMaster.printAllMaterial();
    }
}
