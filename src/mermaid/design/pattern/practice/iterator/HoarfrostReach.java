package mermaid.design.pattern.practice.iterator;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class HoarfrostReach{
    Map<Integer, Material> mapOfMaterial;

    public HoarfrostReach() {
        this.mapOfMaterial = new HashMap<Integer, Material>();
        addMaterial("Fire Herb", 1, "F");
        addMaterial("Flamenut", 2, "&");
        addMaterial("Dragonstrike Nut", 2, "@");
        addMaterial("Snow Herb", 1, "S");
    }
    
    public void addMaterial(String name, int space, String symbol) {
        this.mapOfMaterial.put(Integer.valueOf(this.mapOfMaterial.size()), new Material(name, space, symbol));
    }
    
    public Iterator<Material> createIterator() {
        return new HoarfrostReachIterator(this.mapOfMaterial);
    }
}
