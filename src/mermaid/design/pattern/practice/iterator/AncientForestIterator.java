package mermaid.design.pattern.practice.iterator;

import java.util.Iterator;
import java.util.List;

public class AncientForestIterator implements Iterator<Material> {
    private List<Material> listOfMaterials;
    private int index;
    
    public AncientForestIterator(List<Material> listOfMaterials) {
        super();
        this.listOfMaterials = listOfMaterials;
        this.index = 0;
    }

    @Override
    public boolean hasNext() {
        return (listOfMaterials != null)  && !listOfMaterials.isEmpty() && (index < listOfMaterials.size());
    }

    @Override
    public Material next() {
        Material material = listOfMaterials.get(index);
        index++;
        return material;
    }
}