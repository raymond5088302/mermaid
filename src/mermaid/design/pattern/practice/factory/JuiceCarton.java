package mermaid.design.pattern.practice.factory;

public abstract class JuiceCarton {
    public enum Size {
        SMALL, REGULAR, BIG
    }

    protected String name;
    protected int capacity;
    protected double temperature;

    public boolean isIced() {
        return Double.compare(this.temperature, 4.5) <= 0;
    }

    public JuiceCarton(String name, int capacity, double temperature) {
        this.name = name;
        this.capacity = capacity;
        this.temperature = temperature;
    }

    public Size calculateSize() {
        if (this.capacity <= 250) {
            return Size.SMALL;
        } else if (this.capacity <= 500) {
            return Size.REGULAR;
        } else {
            return Size.BIG;
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        return sb.append("JuiceCarton [name=").append(name).append(", capacity=").append(capacity)
                .append(", temperature=").append(temperature).append("]").toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }
}
