package mermaid.design.pattern.practice.factory;

import java.util.ArrayList;
import java.util.Scanner;
import static java.lang.System.out;

/* Reference to https://www.youtube.com/watch?v=MkCyVId3KY8 */
public class Main {
    public static void main(String[] args) {
        ArrayList<JuiceCarton> cartons = new ArrayList<JuiceCarton>();
        JuiceFactory.Flavor flavor = null;
        
        try (Scanner input = new Scanner(System.in)) {
            loop: do {
                out.println("What type of juice do you want to produce?(Cherry / Orange / Raspberry) Enter S to stop.");
                switch(Character.toUpperCase(input.nextLine().charAt(0))) {
                    case 'C':
                        flavor = JuiceFactory.Flavor.CHERRY;
                        break;
                    case 'O':
                        flavor = JuiceFactory.Flavor.ORANGE;
                        break;
                    case 'R':
                        flavor = JuiceFactory.Flavor.RASPBERRY;
                        break;
                    default:
                        break loop;
                }
                JuiceCarton carton = JuiceFactory.produceJuiceCarton(flavor);
                out.println("Put it into refrigerator?(Y/N)");
                if (Character.toUpperCase(input.nextLine().charAt(0)) == 'Y') {
                    carton.setTemperature(2.5);
                }
                out.println("Enter the value of capacity of box? (mL) accept it less than 2000 mL.");
                int capacity = Integer.parseInt(input.nextLine());
                if ((capacity >= 0) && (capacity <= 2000)) {
                    carton.setCapacity(capacity);
                } else {
                    throw new Exception("The capacity is not allowed.");
                }
                cartons.add(carton);
            } while (true);
        } catch (NumberFormatException e) {
            System.err.println("The value of capacity must be a number.");
        } catch (Exception e) {
            System.err.println(e.getMessage());
        } finally {
            out.println();
            out.println();
        }
        
        out.println("The list of cartons:");
        for(JuiceCarton carton : cartons) {
            demonstrate(carton);
        }
    }
    
    public static void demonstrate(JuiceCarton carton) {
        StringBuilder sb = new StringBuilder();
        sb.append(carton.toString()).append(", Size is ").append(carton.calculateSize());
        if (carton.isIced()) {
            sb.append(", juice is iced");
        }
        out.println(sb.toString());
        if (carton instanceof CherryJuiceCarton) {
            ((CherryJuiceCarton) carton).becomeBored();
        } else if (carton instanceof RaspberryJuiceCarton) {
            ((RaspberryJuiceCarton) carton).showRage();
        } else if (carton instanceof OrangeJuiceCarton) {
            ((OrangeJuiceCarton) carton).startAnnoying();;
        }
        out.println();
        out.println();
    }
}
