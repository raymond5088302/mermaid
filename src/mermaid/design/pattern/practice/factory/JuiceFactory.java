package mermaid.design.pattern.practice.factory;

public class JuiceFactory {
    public enum Flavor {
        CHERRY, RASPBERRY, ORANGE
    }
    
    public static JuiceCarton produceJuiceCarton(Flavor flavor) {
        JuiceCarton carton = null;
        
        switch(flavor) {
            case CHERRY:
                carton = new CherryJuiceCarton("Cherry Juice Carton", 300, 15.0, true);
                break;
            case ORANGE:
                carton = new OrangeJuiceCarton("Orange Juice Carton", 300, 15.0, true);
                break;
            case RASPBERRY: 
                carton = new RaspberryJuiceCarton("Raspberry Juice Carton", 300, 15.0, true);
                break;
        }
        
        return carton;
    }
}
