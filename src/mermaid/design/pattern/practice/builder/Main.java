package mermaid.design.pattern.practice.builder;

// Reference to https://www.youtube.com/watch?v=w7uOhFTrrq0&t
public class Main {

    public static void main(String[] args) {
        Builder forestBuilder = new ForestBuilder(); 
        Creator forestCreator = new Creator(forestBuilder);
        forestCreator.createEcology();
        forestBuilder.paint();
        
        Builder iceborneBuilder = new IceborneBuilder(); 
        Creator iceborneCreator = new Creator(iceborneBuilder);
        iceborneCreator.createEcology();
        iceborneBuilder.paint();
        
    }
}
