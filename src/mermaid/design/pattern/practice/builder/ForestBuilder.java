package mermaid.design.pattern.practice.builder;

public class ForestBuilder extends EcologyBuilder {
    
    public ForestBuilder() {
        
        /* Soil, Water, Tree, Human */
        super(120, 30, new Environment('S', 'W', 'T', 'H'));
    }

}
