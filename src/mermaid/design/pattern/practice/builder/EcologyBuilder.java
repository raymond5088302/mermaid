package mermaid.design.pattern.practice.builder;

import java.util.Arrays;

/* Encapsulate ways of how to create complex object. */
public abstract class EcologyBuilder implements Builder {
    protected char[][] ecology; // this is the complex object I mean
    protected Environment environment;
    protected int edgeX;
    protected int edgeY;
    protected int surfaceY;
    
    public EcologyBuilder(int width, int height, Environment environment) {
        this.ecology = new char[height][width];
        for (char[] row : this.ecology) {
            Arrays.fill(row, ' ');
        }
        this.environment = environment;
        this.edgeX = this.ecology[0].length;
        this.edgeY = this.ecology.length;
        this.surfaceY = this.edgeY - (this.edgeY / 5) - 1;
    }
    
    /*
     * because creator(client) can't directly manipulate the complex object, so this method make sure
     * that the complex object is implemented.
     */
    public char[][] getEcology() {
        return this.ecology;
    }

    public void buildCover() {
       int maxBlockHeight = this.edgeY / 5;
       int maxBlockWidth = this.edgeX / 15;
       int currentX = 0;
       int currentY = this.edgeY - 1;
       
       while ((currentX + maxBlockWidth) < (this.ecology[0].length)) {
            int blockHeight = maxBlockHeight - ((int) (Math.random() * 3));
            int blocWidth = maxBlockWidth - ((int) (Math.random() * (maxBlockWidth - 5)));
            
            if (hasPlace(currentX, currentY - blockHeight, currentX + blocWidth, currentY)) {
                putItems(currentX, currentY - blockHeight, currentX + blocWidth, currentY, this.environment.getCover());
                currentX = currentX + blocWidth + 1;
            }
       }
        
    }

    public void buildLiquid() {
        for (int y = this.surfaceY; y < this.edgeY; y++) {
            for (int x = 0; x < this.edgeX; x++) {
                if (this.ecology[y][x] != this.environment.getCover()) {
                    this.ecology[y][x] = this.environment.getLiquid();
                }
            }
        }
    }

    public void buildPlants() {
        for (int x = 0; x < this.edgeX; x++) {
            if(this.ecology[this.surfaceY][x] == this.environment.getCover()) {
                if ((int) (Math.random() * 5) == 0) {
                    int treeHeight = (int) (Math.random() * 5) + 5;
                    putItems(x, this.surfaceY - treeHeight, x, this.surfaceY - 1, this.environment.getPlant());
                }
            }
        }
    }

    public void buildCreatures() {
        for (int x = 0; x < this.edgeX; x++) {
            if(this.ecology[this.surfaceY][x] == this.environment.getCover()) {
                if (this.ecology[this.surfaceY - 1][x] != this.environment.getPlant()) {
                    if ((int) (Math.random() * 10) == 0) {
                        putItems(x, this.surfaceY - 2, x, this.surfaceY - 1, this.environment.getCreature());
                    }
                }
            }
        }
    }
    
    public boolean hasPlace(int startX, int startY, int endX, int endY) {
        for (int y = startY; y <= endY; y++) {
            for (int x = startX; x <= endX; x++) {
                if (this.ecology[y][x] != ' ') {
                    return false;
                }
            }
        }
        
        return true;
    }
    
    public void putItems(int startX, int startY, int endX, int endY, char item) {
        for (int y = startY; y <= endY; y++) {
            for (int x = startX; x <= endX; x++) {
                this.ecology[y][x] = item;
            }
        }
    }
    
    public void paint() {
        for (char[] row : this.ecology) {
            for (char block : row) {
                System.out.print(block);
            }
            System.out.println();
        }
    }
    
}
