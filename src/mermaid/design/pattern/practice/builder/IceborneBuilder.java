package mermaid.design.pattern.practice.builder;

public class IceborneBuilder extends EcologyBuilder {
    
    public IceborneBuilder() {
        
        /* Ice, Hot Springs, Mushroom, Eskimos */
        super(120, 30, new Environment('I', 'H', 'M', 'E'));
    }
 
}
