package mermaid.design.pattern.practice.builder;

public interface Builder {
    
    public char[][] getEcology();
    public void buildCover();
    public void buildLiquid();
    public void buildPlants();
    public void buildCreatures();
    public void paint();
    
}
  