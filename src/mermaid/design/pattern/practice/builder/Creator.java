package mermaid.design.pattern.practice.builder;

/* control the correct order and run steps to create the complex object by the builder in place of manipulating the complex object straight.    */
public class Creator {

    Builder builder;
    
    public Creator(Builder builder) {
        this.builder = builder;
    }
    
    public void createEcology () {
        this.builder.buildCover();
        this.builder.buildLiquid();
        this.builder.buildPlants();
        this.builder.buildCreatures();
    }
    
    public Builder getBuilder() {
        return this.builder;
    }
    
}
