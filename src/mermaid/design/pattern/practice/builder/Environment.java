package mermaid.design.pattern.practice.builder;

public class Environment {
    private char cover;
    private char liquid;
    private char plant;
    private char creature;
    
    public Environment(char cover, char liquid, char plant, char creature) {
        this.cover = cover;
        this.liquid = liquid;
        this.plant = plant;
        this.creature = creature;
    }

    public char getCover() {
        return cover;
    }

    public void setCover(char cover) {
        this.cover = cover;
    }

    public char getLiquid() {
        return liquid;
    }

    public void setLiquid(char liquid) {
        this.liquid = liquid;
    }

    public char getPlant() {
        return plant;
    }

    public void setPlant(char plant) {
        this.plant = plant;
    }

    public char getCreature() {
        return creature;
    }

    public void setCreature(char creature) {
        this.creature = creature;
    }
    
}
