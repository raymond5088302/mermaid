package mermaid.design.pattern.practice.decorator;

public class Pork extends BurgerDecorator{

    public Pork(Burger burger) {
        super(burger);
        burger.getContents().put("Pork", 50.15);
    }

}
