package mermaid.design.pattern.practice.decorator;

public class Lettuce extends BurgerDecorator{

    public Lettuce(Burger burger) {
        super(burger);
        burger.getContents().put("Lettuce", 7.45);
    }

}
