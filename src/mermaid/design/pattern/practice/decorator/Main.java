package mermaid.design.pattern.practice.decorator;

//idea from https://www.youtube.com/watch?v=xttNbYnBkgQ
public class Main {

    public static void main(String[] args) {
        Burger classicHamburger = new Tomato(new Pork(new Hamburger()));
        System.out.println("A classic hamburger (" + classicHamburger.toString() + ")");

        Burger monsterHamburger = new Tomato(new Lettuce(new Cheese(new Bacon(new Onion(new Octopus(new Pork(new Hamburger())))))));
        System.out.println("A monster hamburger (" + monsterHamburger.toString() + ")");
    }

}
