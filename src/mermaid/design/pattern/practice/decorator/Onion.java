package mermaid.design.pattern.practice.decorator;

public class Onion extends BurgerDecorator{

    public Onion(Burger burger) {
        super(burger);
        burger.getContents().put("Onion", 11.35);
    }

}
