package mermaid.design.pattern.practice.decorator;

public class Bacon extends BurgerDecorator{

    public Bacon(Burger burger) {
        super(burger);
        burger.getContents().put("Bacon", 15.25);
    }

}
