package mermaid.design.pattern.practice.decorator;

public class Tomato extends BurgerDecorator{

    public Tomato(Burger burger) {
        super(burger);
        burger.getContents().put("Tomato", 10.5);
    }

}
