package mermaid.design.pattern.practice.decorator;

public class Cheese extends BurgerDecorator{

    public Cheese(Burger burger) {
        super(burger);
        burger.getContents().put("Cheese", 19.35);
    }

}
