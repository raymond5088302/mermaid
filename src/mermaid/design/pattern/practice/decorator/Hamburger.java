package mermaid.design.pattern.practice.decorator;

import java.util.Map;
import java.util.TreeMap;

public class Hamburger implements Burger {
    private Map<String, Double> contents;
    
    public Hamburger() {
        this.contents = new TreeMap<>();
        this.contents.put("Bun", 25.5);
    }

    @Override
    public Map<String, Double> getContents() {
        return this.contents;
    }
}
