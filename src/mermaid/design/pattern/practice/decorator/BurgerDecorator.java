package mermaid.design.pattern.practice.decorator;

import java.util.Map;

public abstract class BurgerDecorator implements Burger {
    protected Burger burger;
    
    public BurgerDecorator(Burger burger) {
        this.burger = burger;
    }
    
    public Map<String, Double> getContents() {
        return burger.getContents();
    }
    
    public String toString() {
        return String.format(
            "Stuffs: %s, Price: %s", 
            String.join(", ", burger.getContents().keySet()),
            burger.getContents().values().parallelStream().reduce(
                Double.valueOf(0.0),
                (price, unitPrice) -> price + unitPrice
            )
        );
    }
}
