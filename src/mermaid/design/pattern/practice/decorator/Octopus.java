package mermaid.design.pattern.practice.decorator;

public class Octopus extends BurgerDecorator {
    
    public Octopus(Burger burger) {
        super(burger);
        burger.getContents().put("Octopus", 121.85);
    }
}
