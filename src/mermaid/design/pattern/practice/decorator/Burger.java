package mermaid.design.pattern.practice.decorator;

import java.util.Map;

public interface Burger {
    public Map<String, Double> getContents();
}
