package mermaid.design.pattern.practice.singleton;

import static java.lang.System.out;

/* reference to https://www.youtube.com/watch?v=ZpKzTdMSaJs */
public class Main {

    public static void main(String[] args) {
        Kitchen kitchen01 = Kitchen.getInstance();
        out.println("The rest of vegetables are " + kitchen01.getListOfVegetables());
        out.println("The rest of fruits are " + kitchen01.getListOfFruits());
        out.println();
        out.println("Hashcode of kitchen01: " + System.identityHashCode(kitchen01));
        out.println("The recipe for salad: " + kitchen01.getRecipeForSalad());
        out.println("The recipe for smoothie: " + kitchen01.getRecipeForSmoothie());
        out.println();
        
        
        Kitchen kitchen02 = Kitchen.getInstance();
        out.println("The rest of vegetables are " + kitchen02.getListOfVegetables());
        out.println("The rest of fruits are " + kitchen02.getListOfFruits());
        out.println();
        out.println("Hashcode of kitchen02: " + System.identityHashCode(kitchen02));
        out.println("The recipe for salad: " + kitchen02.getRecipeForSalad());
        out.println("The recipe for smoothie: " + kitchen02.getRecipeForSmoothie());
    }

}
