package mermaid.design.pattern.practice.singleton;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

public class Kitchen {
    private static Kitchen instanceOfKitchen = null;
    private String[] vegetables = {"lettuce", "tomato", "onion", "carrot", "spinach", "cauliflower", "broccoli", "asparagus"};
    private String[] fruits = {"strawberry", "kiwi", "raspberry", "blueberry", "banana", "apple", "orange", "apple", "pear", "passionfruit", "mango", "peach"};
    private LinkedList<String> listOfVegetables = new LinkedList<String>(Arrays.asList(vegetables));
    private LinkedList<String> listOfFruits = new LinkedList<String>(Arrays.asList(fruits));
    
    private Kitchen() {
        
    }
    
    public static Kitchen getInstance() {
        synchronized(Kitchen.class) { 
            if (instanceOfKitchen == null) {
                instanceOfKitchen = new Kitchen();
            }
            Collections.shuffle(instanceOfKitchen.listOfVegetables);
            Collections.shuffle(instanceOfKitchen.listOfFruits);
        }
        
        return instanceOfKitchen;
    }
    
    public LinkedList<String> getRecipeForSalad() {
        LinkedList<String> recipeForSalad = new LinkedList<String>();
        for (int i = 0; i < 3; i++) {
            recipeForSalad.add(listOfVegetables.remove(0));
        }
        
        return recipeForSalad;
    }
    
    public LinkedList<String> getListOfVegetables() {
        return listOfVegetables;
    }

    public LinkedList<String> getListOfFruits() {
        return listOfFruits;
    }

    public LinkedList<String> getRecipeForSmoothie() {
        LinkedList<String> recipeForSalad = new LinkedList<String>();
        for (int i = 0; i < 3; i++) {
            recipeForSalad.add(listOfFruits.remove(0));
        }
        
        return recipeForSalad;
    }
    
}
