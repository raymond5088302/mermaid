package mermaid.design.pattern.practice.strategy2;

public class AngryApple extends Fruit {
    
    public AngryApple(String name, double weight, int wordsPerMinute, String petPhrase, boolean canMove) {
        super(name, weight, wordsPerMinute, petPhrase, canMove);
    }
    
    public void angry(Fruit fruit) {
        System.out.printf("%s is angry at %s.%n", this.name, fruit.name);
    }
}
