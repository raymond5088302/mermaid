package mermaid.design.pattern.practice.strategy2;

/* Reference to https://www.youtube.com/watch?v=DD5UKQggXTc */
public class Main {

    public static void main(String[] args) {
        AnnoyingOrange annoyingOrange = new AnnoyingOrange("AnnoyingOrange", 250, 180, "Hey!!", false);
        AngryApple angryApple = new AngryApple("AngryApple", 280, 90, "What is it!!", false);

        annoyingOrange.annoy(angryApple);
        angryApple.angry(annoyingOrange);
        annoyingOrange.setAbilityToTongueOut((hasReachedNose, times) -> (new StringBuffer()).append(" can touch out ")
                .append(hasReachedNose ? "and reach his self nose " : "").append(times).append(" times!!").toString());
        angryApple.setAbilityToTongueOut(
                (hasReachedNose, times) -> (new StringBuffer()).append(" can not touch out!!").toString());
        annoyingOrange.showMeTongueOut();
        angryApple.showMeTongueOut();
    }

}
