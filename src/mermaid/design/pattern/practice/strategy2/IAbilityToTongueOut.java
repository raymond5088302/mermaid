package mermaid.design.pattern.practice.strategy2;

@FunctionalInterface
public interface IAbilityToTongueOut {
    String tongueOut(boolean hasReachedNose, int times);
}
