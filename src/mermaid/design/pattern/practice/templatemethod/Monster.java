package mermaid.design.pattern.practice.templatemethod;

public abstract class Monster {
    
    public final void performCombos() {
        move();
        basicAttack();
        rush();
        produceLaserBeam();
        specialAttack();
        specialAttack();
    }
    
    public abstract void move();
    public abstract void specialAttack();
    
    public void basicAttack() {
        System.out.printf("%s attack the hunter!%n", this.getClass().getSimpleName());
    }
    
    public void produceLaserBeam() {
        System.out.printf("%s giving the hunter a super laser beam!%n", this.getClass().getSimpleName());
    }
    
    public void rush() {
        System.out.printf("%s starts preparing to rush the hunter!%n", this.getClass().getSimpleName());
    }
    
}
