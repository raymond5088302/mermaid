package mermaid.design.pattern.practice.templatemethod;

public class AdultXenojiiva extends Monster{
    public enum SpecialAttack {
        BITE(0), BREAKTHEGROUND(1), BURNTHEGROUND(2);
        
        private final int value;
        
        private SpecialAttack(int value) {
            this.value = value;
        }
        
        public String toString() {
            String result = null;
            switch(this) {
                default:
                case BITE:
                    result = "%s is eating the unlucky hunter then spit out!%n";
                    break;
                case BREAKTHEGROUND:
                    result = "%s is tapping and breaking the ground!%n";
                    break;
                case BURNTHEGROUND:
                    result = "%s fly to sky and prepare to burn all hunters!%n";
                    break;
            }
            
            return result;
        }
        
        public static SpecialAttack valueOf(int rand) {
            for (SpecialAttack attack: values()) {
                if (rand == attack.value) {
                    return attack;
                }
            }
            
            return BITE;
        }
    }
    
    @Override
    public void move() {
        System.out.printf("%s starts flying!%n", this.getClass().getSimpleName());
    }

    @Override
    public void basicAttack() {
        System.out.printf("%s attacking the hunter!%n", this.getClass().getSimpleName());
        
    }

    @Override
    public void specialAttack() {
        System.out.printf(SpecialAttack.valueOf((int)(Math.random() * 3)).toString(), this.getClass().getSimpleName());
    }

}
