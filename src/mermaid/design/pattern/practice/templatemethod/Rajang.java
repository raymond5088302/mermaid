package mermaid.design.pattern.practice.templatemethod;

public class Rajang extends Monster{
    public enum SpecialAttack {
        SERIALPUNCH(0), THROWOUT(1);
        
        private final int value;
        
        private SpecialAttack(int value) {
            this.value = value;
        }
        
        public String toString() {
            String result = null;
            switch(this) {
                default:
                case SERIALPUNCH:
                    result = "%s serially punching the hunter!%n";
                    break;
                case THROWOUT:
                    result = "%s throwing the hunter out heavily!%n";
                    break;
            }
            
            return result;
        }
        
        public static SpecialAttack valueOf(int rand) {
            for (SpecialAttack attack: values()) {
                if (rand == attack.value) {
                    return attack;
                }
            }
            
            return SERIALPUNCH;
        }
    }
    

    @Override
    public void move() {
        System.out.printf("%s starts jumping!%n", this.getClass().getSimpleName());
    }


    @Override
    public void specialAttack() {
        System.out.printf(SpecialAttack.valueOf((int)(Math.random() * 2)).toString(), this.getClass().getSimpleName());
    }

}
