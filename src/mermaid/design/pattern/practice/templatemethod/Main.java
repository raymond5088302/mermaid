package mermaid.design.pattern.practice.templatemethod;

/**
 * Adult Xeno'jiiva: https://youtu.be/XX1fPsCgK-g?t=164
 * Rajang: https://www.youtube.com/watch?v=e-rtlKzNA7M
 */
public class Main {
    public static void main(String args[]) {
        (new AdultXenojiiva()).performCombos();
        System.out.println();
        (new Rajang()).performCombos();
    }
}
