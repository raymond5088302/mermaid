package mermaid.design.pattern.practice.prototype1;

public interface Fruit extends Cloneable {
    public Fruit copycat();
}
