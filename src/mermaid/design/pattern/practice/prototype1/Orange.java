package mermaid.design.pattern.practice.prototype1;

public class Orange implements Fruit {
    private int weight;
    
    public Orange(int weight) {
        this.weight = weight;
    }

    @Override
    public Fruit copycat() {
        Fruit clone = null;
        
        try {
            clone = (Orange) super.clone();
            System.out.println("Orange's clone have been made!");
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        
        return clone;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Orange [weight=");
        builder.append(weight);
        builder.append("]");
        
        return builder.toString();
    }

}
