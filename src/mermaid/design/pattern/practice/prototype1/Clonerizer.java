package mermaid.design.pattern.practice.prototype1;

public class Clonerizer {
    public static Fruit getClone(Fruit prototype) {
        return prototype.copycat();
    }
}
