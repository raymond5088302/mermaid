package mermaid.design.pattern.practice.prototype1;

/* idea from https://www.youtube.com/watch?v=jvnKybI7fzU */
public class Main {

    public static void main(String[] args) {
        Fruit originalAnnoyingOrange = new Orange(30);
        Fruit secondAnnoyingOrange = Clonerizer.getClone(originalAnnoyingOrange);
        
        System.out.printf("The content of original AnnoyingOrange: %s%n", originalAnnoyingOrange);
        System.out.printf("The content of AnnoyingOrange's clone: %s%n", secondAnnoyingOrange);
        System.out.printf("The hashcode of original AnnoyingOrange: %d%n", System.identityHashCode(originalAnnoyingOrange));
        System.out.printf("The hashcode of AnnoyingOrange's clone: %d%n", System.identityHashCode(secondAnnoyingOrange));
    }

}
