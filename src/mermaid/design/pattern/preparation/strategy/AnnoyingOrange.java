package mermaid.design.pattern.preparation.strategy;

public class AnnoyingOrange extends Fruit {
    
    public AnnoyingOrange(String name, double weight, int wordsPerMinute, String petPhrase, boolean canMove) {
        super(name, weight, wordsPerMinute, petPhrase, canMove);
    }
    
    public void annoy(Fruit fruit) {
        System.out.printf("%s is annoying %s.%n", this.name, fruit.name);
    }
}
