package mermaid.design.pattern.preparation.strategy;

public class Fruit {
    protected String name;
    protected double weight;
    protected int wordsPerMinute;
    protected String petPhrase;
    protected boolean canMove;
    
    public Fruit(String name, double weight, int wordsPerMinute, String petPhrase, boolean canMove) {
        this.name = name;
        this.weight = weight;
        this.wordsPerMinute = wordsPerMinute;
        this.petPhrase = petPhrase;
        this.canMove = canMove;
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public double getWeight() {
        return weight;
    }
    public void setWeight(double weight) {
        this.weight = weight;
    }
    public int getWordsPerMinute() {
        return wordsPerMinute;
    }
    public void setSpeedPerMinute(int wordsPerMinute) {
        this.wordsPerMinute = wordsPerMinute;
    }
    public String getPetPhrase() {
        return petPhrase;
    }
    public void setPetPhrase(String petPhrase) {
        this.petPhrase = petPhrase;
    }
    public boolean isCanMove() {
        return canMove;
    }
    public void setCanMove(boolean canMove) {
        this.canMove = canMove;
    }
}
