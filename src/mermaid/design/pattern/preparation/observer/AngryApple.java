package mermaid.design.pattern.preparation.observer;

public class AngryApple extends Fruit{

    public AngryApple() {
        super("AngryApple", "What is it!!", false, true);
    }

}
