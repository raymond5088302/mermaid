package mermaid.design.pattern.preparation.observer;

public class AnnoyingOrange extends Fruit {

    public AnnoyingOrange() {
        super("AnnoyingOrange", "Hey!!", true, false);
    }

}
