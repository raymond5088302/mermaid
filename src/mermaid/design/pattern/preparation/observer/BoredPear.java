package mermaid.design.pattern.preparation.observer;

public class BoredPear extends Fruit {

    public BoredPear() {
        super("BoredPear", "Oh! God!", false, true);
    }

}
