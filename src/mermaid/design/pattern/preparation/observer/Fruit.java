package mermaid.design.pattern.preparation.observer;

public class Fruit {
    protected String name;
    protected String petPhrase;
    protected boolean isAnnoying;
    protected boolean isAngry;
    
    public Fruit(String name, String petPhrase, boolean isAnnoying, boolean isAngry) {
        this.name = name;
        this.petPhrase = petPhrase;
        this.isAnnoying = isAnnoying;
        this.isAngry = isAngry;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPetPhrase() {
        return petPhrase;
    }

    public void setPetPhrase(String petPhrase) {
        this.petPhrase = petPhrase;
    }

    public boolean isAnnoying() {
        return isAnnoying;
    }

    public void setAnnoying(boolean isAnnoying) {
        this.isAnnoying = isAnnoying;
    }

    public boolean isAngry() {
        return isAngry;
    }

    public void setAngry(boolean isAngry) {
        this.isAngry = isAngry;
    }

  
}
