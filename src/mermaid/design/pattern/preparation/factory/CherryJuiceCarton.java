package mermaid.design.pattern.preparation.factory;

public class CherryJuiceCarton extends JuiceCarton {
    protected boolean hasBecomeBored;
    
    public CherryJuiceCarton(String name, int capacity, double temperature, boolean hasBecomeBored) {
        super(name, capacity, temperature);
        this.hasBecomeBored = hasBecomeBored;
    }
    
    public void becomeBored() {
        if (hasBecomeBored) {
            System.out.printf("%s is becoming Bored now!!", super.name);            
        } else {
            System.out.printf("%s keeps things interesting!!", super.name);  
        }
    }
}
