package mermaid.design.pattern.preparation.factory;

/* Reference to https://www.youtube.com/watch?v=MkCyVId3KY8 */
public class Main {
    public static void main(String[] args) {
        CherryJuiceCarton cherry = new CherryJuiceCarton("Cherry Juice Carton", 350, 3.0, true);
        RaspberryJuiceCarton raspberry = new RaspberryJuiceCarton("Raspberry Juice Carton", 600, -6.3, true);
        OrangeJuiceCarton orange = new OrangeJuiceCarton("Orange Juice Carton", 150, 20.0, true);
        demonstrate(cherry);
        demonstrate(raspberry);
        demonstrate(orange);
    }
    
    public static void demonstrate(JuiceCarton carton) {
        StringBuilder sb = new StringBuilder();
        sb.append(carton.toString()).append(", Size is ").append(carton.calculateSize());
        if (carton.isIced()) {
            sb.append(", juice is iced");
        }
        System.out.println(sb.toString());
        if (carton instanceof CherryJuiceCarton) {
            ((CherryJuiceCarton) carton).becomeBored();
        } else if (carton instanceof RaspberryJuiceCarton) {
            ((RaspberryJuiceCarton) carton).showRage();
        } else if (carton instanceof OrangeJuiceCarton) {
            ((OrangeJuiceCarton) carton).startAnnoying();;
        }
        System.out.println();
        System.out.println();
    }
}
