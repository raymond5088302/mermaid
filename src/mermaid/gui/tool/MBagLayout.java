package mermaid.gui.tool;

import java.awt.*;
import javax.swing.*;

public class MBagLayout {
    
    public static void addCompoment(JPanel panel, JComponent component, int xPos, int yPos, int width, int height, int anchor, int strech) {
        addCompoment(panel, component, xPos, yPos, width, height, anchor, strech, 0, 0);
    }

    public static void addCompoment(JPanel panel, JComponent component, int xPos, int yPos, int width, int height, int anchor, int strech, int xWeight, int yWeight) {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = xPos; 
        constraints.gridy = yPos;
        constraints.gridwidth = width;
        constraints.gridheight = height;
        constraints.weightx = xWeight;
        constraints.weighty = yWeight;
        constraints.anchor = anchor;
        constraints.fill = strech;
        constraints.insets = new Insets(5, 5, 5, 5);
        panel.add(component, constraints);
    }
}
