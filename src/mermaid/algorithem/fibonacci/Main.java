package mermaid.algorithem.fibonacci;

public class Main {

    public static void main(String[] args) {
        int n = 12;
        long total = 0;
        long milliseconds = System.currentTimeMillis();
        long runningTime = 0;
        
        total = Mathematics.formula01(n);
        runningTime = System.currentTimeMillis() - milliseconds;
        System.out.println("formal01, n: " + n + ", total: " + total + ", running time(ms): " + runningTime);
        milliseconds = System.currentTimeMillis();
        Mathematics.formula02(n);
        runningTime = System.currentTimeMillis() - milliseconds;
        System.out.println("formal02, n: " + n + ", total: " + total + ", running time(ms): " + runningTime);
        milliseconds = System.currentTimeMillis();
        Mathematics.formula03(n);
        runningTime = System.currentTimeMillis() - milliseconds;
        System.out.println("formal03, n: " + n + ", total: " + total + ", running time(ms): " + runningTime);
    }

}
