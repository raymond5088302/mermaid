package mermaid.algorithem.fibonacci;

import java.awt.*;
import java.awt.event.*;
import java.text.NumberFormat;

import javax.swing.*;
import mermaid.gui.tool.*;

public class GUIMain extends JFrame {
    JLabel numLabel;
    JSpinner numSpinner;
    JCheckBox formula01CheckBox, formula02CheckBox, formula03CheckBox;
    JLabel formula01ResultLabel, formula02ResultLabel, formula03ResultLabel;
    JLabel formula01SpendingLabel, formula02SpendingLabel, formula03SpendingLabel;
    JButton testBtn;
    
    public static void main(String[] args) {
        new GUIMain();
    }
    
    public GUIMain() {
        GridBagLayout mainLayout = new GridBagLayout();
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(mainLayout);
        
        numLabel = new JLabel("Number: ");
        SpinnerNumberModel numberModel = new SpinnerNumberModel(8, 0, 85, 1);
        numSpinner = new JSpinner(numberModel);
        MBagLayout.addCompoment(mainPanel, numLabel, 0, 0, 1, 1, GridBagConstraints.EAST, GridBagConstraints.NONE);
        MBagLayout.addCompoment(mainPanel, numSpinner, 1, 0, 3, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL);
        formula01CheckBox = new JCheckBox("Recursion:");
        formula02CheckBox = new JCheckBox("Formula:");
        formula03CheckBox = new JCheckBox("Loop:");
        formula01CheckBox.setSelected(true);
        formula02CheckBox.setSelected(true);
        formula03CheckBox.setSelected(true);
        MBagLayout.addCompoment(mainPanel, formula01CheckBox, 0, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE);
        MBagLayout.addCompoment(mainPanel, formula02CheckBox, 0, 2, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE);
        MBagLayout.addCompoment(mainPanel, formula03CheckBox, 0, 3, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE);
        formula01ResultLabel = new JLabel("Result:");
        formula02ResultLabel = new JLabel("Result:");
        formula03ResultLabel = new JLabel("Result:");
        MBagLayout.addCompoment(mainPanel, formula01ResultLabel, 1, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.NONE);
        MBagLayout.addCompoment(mainPanel, formula02ResultLabel, 1, 2, 1, 1, GridBagConstraints.EAST, GridBagConstraints.NONE);
        MBagLayout.addCompoment(mainPanel, formula03ResultLabel, 1, 3, 1, 1, GridBagConstraints.EAST, GridBagConstraints.NONE);
        formula01SpendingLabel = new JLabel("Running Time(ms):");
        formula02SpendingLabel = new JLabel("Running Time(ms):");
        formula03SpendingLabel = new JLabel("Running Time(ms):");
        MBagLayout.addCompoment(mainPanel, formula01SpendingLabel, 2, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE);
        MBagLayout.addCompoment(mainPanel, formula02SpendingLabel, 2, 2, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE);
        MBagLayout.addCompoment(mainPanel, formula03SpendingLabel, 2, 3, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE);
        testBtn = new JButton("Try Me Now");
        testBtn.addActionListener(new ListenForTestBtn());
        MBagLayout.addCompoment(mainPanel, testBtn, 0, 4, 3, 1, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL);
        
        this.add(mainPanel);
        this.pack();
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setTitle("Fibonacci");
        this.setVisible(true);
    }
    
    private class ListenForTestBtn implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            if (event.getSource() == testBtn) {
                int num = (int) numSpinner.getValue();
                long total = 0;;
                long milliseconds = 0;
                long runningTime = 0;
                NumberFormat numberFormat = NumberFormat.getInstance();
                
                if (formula01CheckBox.isSelected()) {
                    milliseconds = System.currentTimeMillis();
                    total = Mathematics.formula01(num);
                    runningTime = System.currentTimeMillis() - milliseconds;
                    formula01ResultLabel.setText("Result: " + numberFormat.format(total));
                    formula01SpendingLabel.setText("Running Time(ms): " + Long.toString(runningTime));
                } else {
                    formula01ResultLabel.setText("Result:");
                    formula01SpendingLabel.setText("Running Time(ms):");
                }
                
                if (formula02CheckBox.isSelected()) {
                    milliseconds = System.currentTimeMillis();
                    total = Mathematics.formula02(num);
                    runningTime = System.currentTimeMillis() - milliseconds;
                    formula02ResultLabel.setText("Result: " + numberFormat.format(total));
                    formula02SpendingLabel.setText("Running Time(ms): " + Long.toString(runningTime));
                } else {
                    formula02ResultLabel.setText("Result:");
                    formula02SpendingLabel.setText("Running Time(ms):");
                }
                
                if (formula03CheckBox.isSelected()) {
                    milliseconds = System.currentTimeMillis();
                    total = Mathematics.formula03(num);
                    runningTime = System.currentTimeMillis() - milliseconds;
                    formula03ResultLabel.setText("Result: " + numberFormat.format(total));
                    formula03SpendingLabel.setText("Running Time(ms): " + Long.toString(runningTime));
                } else {
                    formula03ResultLabel.setText("Result:");
                    formula03SpendingLabel.setText("Running Time(ms):");
                }
                
                GUIMain.this.pack();
            }
        }
    }

}
