package mermaid.algorithem.fibonacci;

public class Mathematics {

    public static long formula01(int month) {
        if ((month == 0) || (month == 1)) {
            return month;
        } else {
            return formula01(month - 1) + formula01(month - 2);
        }
    }
    
    public static long formula02(int month) {
        double total = (1 / Math.sqrt(5) * Math.pow((1 + Math.sqrt(5)) / 2, month)
                - (1 / Math.sqrt(5)) * Math.pow((1 - Math.sqrt(5)) / 2, month));    
        return (long) (total + (1E-16));
    }

    /**
     * Every rabbit needs to wait one month to develop into "adult". "adults" will
     * have their "youngs" next month. After "youngs" pass through one month, they
     * will become adults and produce their youngs next month.
     * 
     * @param n how many months you go through
     * @return the total of rabbit
     */
    public static long formula03(int month) {
        if ((month == 0) || (month == 1)) {
            return month;
        }
        long total = 1;
        long adults = 0;
        long youngs = 0;
        for (int now = 2; now <= month; now++) {
            /* young rabbits from last month will become mature this month */
            adults = total;
            /* adults plus youngs from last month equals the total of rabbit */
            total = adults + youngs;
            /* mature rabbits will reproduce young rabbits next month */
            youngs = adults;
        }

        return total;
    }
}
