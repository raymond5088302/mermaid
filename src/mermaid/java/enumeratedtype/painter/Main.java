package mermaid.java.enumeratedtype.painter;

public class Main {

    public static void main(String[] args) {
        System.out.printf("normal: %s, darker: %x, brighter: %x, flipped color: %x%n",
            Color.GREEN, 
            Color.GREEN.becomeDarker(50),
            Color.GREEN.becomeBrighter(50), 
            Color.GREEN.flip());
        
        Painter painter = Color.GREEN;
        System.out.printf("normal: %s, darker: %x, brighter: %x, flipped color: %x%n",
            painter, 
            painter.becomeDarker(50),
            painter.becomeBrighter(50), 
            painter.flip());
    }

}
