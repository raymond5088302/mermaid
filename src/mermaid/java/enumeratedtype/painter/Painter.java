package mermaid.java.enumeratedtype.painter;

public interface Painter {
    public int becomeDarker(int degree);
    public int becomeBrighter(int degree);
    public int flip();
}
