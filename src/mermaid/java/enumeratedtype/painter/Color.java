package mermaid.java.enumeratedtype.painter;

public enum Color implements Painter {
    ORANGE(0xFF8040), RED(0xAE0000), GREEN(0x82D900);
    
    private int value;
    
    private Color(int value) {
        this.value = value;
    }
    
    public int getValue() {
        return value;
    }
    

    @Override
    public int becomeDarker(int degree) {
        short[] bytes = new short[3];
        int newColor = 0;
        for (int i = 0, bitMove = 0; i < bytes.length; i++, bitMove+=8) {
            if ((((this.value >> bitMove) & 255) - degree) >= 0) {
                bytes[i] = (short)(((this.value >> bitMove) & 255) - degree);
            } else {
                bytes[i] = 0;
            }
        }
        for (int i = 0, bitMove = 0; i < bytes.length; i++, bitMove+=8) {
            newColor += (int)((bytes[i] << bitMove));
        }
        return newColor;
    }

    @Override
    public int becomeBrighter(int degree) {
        short[] bytes = new short[3];
        int newColor = 0;
        for (int i = 0, bitMove = 0; i < bytes.length; i++, bitMove+=8) {
            if ((((this.value >> bitMove) & 255) + degree) <= 255) {
                bytes[i] = (short)(((this.value >> bitMove) & 255) + degree);
            } else {
                bytes[i] = 255;
            }
        }
        for (int i = 0, bitMove = 0; i < bytes.length; i++, bitMove+=8) {
            newColor += (int)((bytes[i] << bitMove));
        }
        
        return newColor;
    }


    @Override
    public int flip() {
        int value = this.value;
        int reverse = 0;
        
        for (int i = 0, mask = 1; i < 24; i++, mask <<= 1) {
            reverse = reverse | (mask ^ (value & mask)) ;
        }
        
        return reverse;
    }
    
    @Override
    public String toString() {
        return Integer.toHexString(this.value);
    }
    
}
