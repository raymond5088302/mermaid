package mermaid.java.reflection.boxedtype;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Parser {
    public static Object parse(Class<?> typeClass, String value) {
        Object o = null;
        
        try {
            Method m = typeClass.getDeclaredMethod("valueOf", String.class);
            o = m.invoke(typeClass, value);
        } catch(NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            e.printStackTrace();
        }
        
        return o;
    }
}
