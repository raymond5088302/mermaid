package mermaid.java.reflection.boxedtype;

public class Main {

    public static void main(String[] args) {
        Double d = (Double)Parser.parse(Double.class, "1.25");
        Integer n = (Integer)Parser.parse(Integer.class, "5");
        System.out.println(d);
        System.out.println(n);
    }

}
