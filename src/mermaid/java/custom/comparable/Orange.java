package mermaid.java.custom.comparable;

public class Orange implements Comparable<Orange>, Fruit {
    private String name;
    private int annoyingDegree;
    
    public Orange(String name, int annoyingDegree) {
        this.name = name;
        this.annoyingDegree = annoyingDegree;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[name=");
        builder.append(name);
        builder.append(", annoyingDegree=");
        builder.append(annoyingDegree);
        builder.append("]");
        return builder.toString();
    }

    public int getAnnoyingDegree() {
        return annoyingDegree;
    }

    @Override
    public int compareTo(Orange another) {
        return -(this.annoyingDegree - another.annoyingDegree);
    }
    
}
