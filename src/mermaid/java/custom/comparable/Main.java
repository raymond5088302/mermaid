package mermaid.java.custom.comparable;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

// an idea from https://www.youtube.com/watch?v=Xp9f7Gn4AQw
public class Main {

    public static void main(String[] args) {
        List oranges = Arrays.asList(
            new Orange("Original Orange", 1),
            new Orange("Orange No.2", 10),
            new Orange("Orange No.3", 3),
            new Orange("Orange No.4", 9),
            new Orange("Orange No.5", 5)
        );
        Collections.sort(oranges);
        oranges.forEach(Main::findCrazyAnnoyingOrange);
        
    }
    
    public static void findCrazyAnnoyingOrange(Object obj) {
        
//        if (obj.getClass().equals(Orange.class)) {
//            if (((Orange) obj).getAnnoyingDegree() > 8) {
//                System.out.println(obj.toString());
//            }
//        }
        
        if (obj instanceof Fruit) {
            if (((Fruit) obj).getAnnoyingDegree() > 8) {
                System.out.println(obj.toString());
            }
        }
    }
}
