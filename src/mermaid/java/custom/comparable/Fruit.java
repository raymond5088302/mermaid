package mermaid.java.custom.comparable;

public interface Fruit {
    int getAnnoyingDegree();
}
