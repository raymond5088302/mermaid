# About Mermaid

Do you like starbucks? 

This repository is for collection of small programs in Java like algorithms, practice for coding and testing programs.

## Client Development Environment 
- MacOS v10.14.3 
- JDK 12.0.2

## Packages

### mermaid.design.pattern
Demonstrate the GoF 23 classic design patterns.

- Step01 .preparation.strategy1: Preparation for strategy pattern
- Step02 .practice.strategy: Put into practice of programming strategy pattern
- Step03 .preparation.observer: Preparation for observer pattern
- Step04 .practice.observer: Put into practice of programming observer pattern
- Step05 .preparation.factory: Preparation for factory pattern
- Step06 .practice.factory: Put into practice of programming factory pattern
- Step07 .preparation.strategy2: Continuing with step2(.practice.strategy1) to use anonymous functions and lambda to rewrite the code.
- Step08 .practice.abstractfactory: Continuing with step6(.practice.factory) to program abstract factory pattern
- Step09 .practice.singleton: Put into practice of programming singleton pattern
- Step10 .practice.builder: Put into practice of programming builder pattern
- Step11 .practice.prototype1: Put into practice of programming prototype pattern with shallow copy
- Step12 .practice.prototype2: Put into practice of programming prototype pattern with shallow copy and using stream handle a large number of duplicate
- Step13 .practice.prototype3: Put into practice of programming prototype pattern with deep copy
- Step14 .practice.decorator: Put into practice of programming decorator pattern
- Step15 .practice.command: Put into practice of programming command pattern
- Step16 .practice.adapter: Continuing with step15(.practice.command) to add adapter pattern
- Step17 .practice.facade: Put into practice of programming facade pattern
- Step18 .practice.bridge: Put into practice of programming bridge pattern
- Step19 .practice.templatemethod: Put into practice of programming template method pattern
- Step20 .practice.iterator: Put into practice of programming iterator pattern
- Step21 .practice.composite: Put into practice of programming composite pattern
- Step22 .practice.flyweight: Put into practice of programming flyweight pattern
- Step23 .practice.state: Put into practice of programming state pattern

## mermaid.java
Examples like tailored programs overriding standard library and advanced utilization so as to make ready for more complex usage.  

### mermaid.algorithem
Demonstrate programs about algorithms

### gui.tool
The tools of Graphical User Interface